package ru.nsu.ccfit.mankiapp.api.data.mapper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import ru.nsu.ccfit.mankiapp.exceptions.BadRequestException;
import ru.nsu.ccfit.mankiapp.exceptions.BaseException;
import ru.nsu.ccfit.mankiapp.exceptions.NotFoundException;
import ru.nsu.ccfit.mankiapp.exceptions.WrongCredentialsException;
import ru.nsu.ccfit.mankiapp.exceptions.security.InvalidTokenException;

public class StatusMapperTest {
    @Test
    void testInvalidTokenError() {
        BaseException e = new InvalidTokenException("");
        Assertions.assertEquals(StatusMapper.mapStatus(e), HttpStatus.FORBIDDEN);
    }

    @Test
    void testNotFoundError() {
        BaseException e = new NotFoundException("");
        Assertions.assertEquals(StatusMapper.mapStatus(e), HttpStatus.NOT_FOUND);
    }

    @Test
    void testBadRequest() {
        BaseException e = new BadRequestException("");
        Assertions.assertEquals(StatusMapper.mapStatus(e), HttpStatus.BAD_REQUEST);
    }

    @Test
    void testInvalidTokenErrorValue() {
        BaseException e = new InvalidTokenException("");
        Assertions.assertEquals(StatusMapper.mapStatusValue(e), 401);
    }

    @Test
    void testWrongCredentials() {
        BaseException e = new WrongCredentialsException("");
        Assertions.assertEquals(StatusMapper.mapStatusValue(e), 401);
    }

    @Test
    void testNotFoundValue() {
        BaseException e = new NotFoundException("");
        Assertions.assertEquals(StatusMapper.mapStatusValue(e), 404);
    }

    @Test
    void testBadRequestValue() {
        BaseException e = new BadRequestException("");
        Assertions.assertEquals(StatusMapper.mapStatusValue(e), 500);
    }
}
