package ru.nsu.ccfit.mankiapp.dto.mapper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.nsu.ccfit.mankiapp.api.data.request.Difficulty;
import ru.nsu.ccfit.mankiapp.api.data.request.PointsRequest;
import ru.nsu.ccfit.mankiapp.api.data.request.TrainTimeRequest;
import ru.nsu.ccfit.mankiapp.dto.PointsAnkiDto;
import ru.nsu.ccfit.mankiapp.dto.PointsIntegralDto;
import ru.nsu.ccfit.mankiapp.dto.TrainTimeDto;

import static org.junit.jupiter.api.Assertions.*;

class TrainPointsMapperTest {

    @Test
    void tryToInstantiate() {
        Assertions.assertThrows(
                IllegalStateException.class,
                TrainPointsMapper::new,
                "Mapper class cannot be instantiated"
        );
    }

    @Test
    void requestToAnkiDto() {
        var request = new PointsRequest(
                1,
                2,
                Difficulty.EASY
        );
        var rightResult = new PointsAnkiDto(
                1,
                2,
                Difficulty.EASY
        );
        Assertions.assertEquals(
                rightResult,
                TrainPointsMapper.requestToAnkiDto(request)
        );
    }

    @Test
    void requestToIntegralDto() {
        var request = new PointsRequest(
                1,
                2,
                Difficulty.EASY
        );
        var rightResult = new PointsIntegralDto(
                1,
                2,
                Difficulty.EASY
        );
        Assertions.assertEquals(
                rightResult,
                TrainPointsMapper.requestToIntegralDto(request)
        );
    }

    @Test
    void requestTimeToDto() {
        var request = new TrainTimeRequest(
                1,
                2,
                "bebra",
                15.0
        );
        var rightResult = new TrainTimeDto(
                1,
                2,
                "bebra",
                15.0
        );
        Assertions.assertEquals(
                rightResult,
                TrainPointsMapper.requestTimeToDto(request)
        );
    }
}