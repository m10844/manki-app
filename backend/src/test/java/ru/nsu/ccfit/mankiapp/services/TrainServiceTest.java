package ru.nsu.ccfit.mankiapp.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import ru.nsu.ccfit.mankiapp.api.data.request.Difficulty;
import ru.nsu.ccfit.mankiapp.dto.PointsAnkiDto;
import ru.nsu.ccfit.mankiapp.dto.PointsIntegralDto;
import ru.nsu.ccfit.mankiapp.dto.TrainTimeDto;
import ru.nsu.ccfit.mankiapp.exceptions.InvalidTrainingTypeException;
import ru.nsu.ccfit.mankiapp.exceptions.NotFoundException;
import ru.nsu.ccfit.mankiapp.exceptions.WrongDeckCardRelationException;
import ru.nsu.ccfit.mankiapp.model.data.*;
import ru.nsu.ccfit.mankiapp.model.repo.DeckRepository;
import ru.nsu.ccfit.mankiapp.model.repo.PublicDecksStatsRepository;
import ru.nsu.ccfit.mankiapp.model.repo.UserPublicDeckScoreRepository;
import ru.nsu.ccfit.mankiapp.model.repo.security.UserRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@MockitoSettings(strictness = Strictness.LENIENT)
public class TrainServiceTest {

    @Mock
    private DeckRepository deckRepository;
    @Mock
    private UserRepository userRepository;
    @Mock
    private PublicDecksStatsRepository publicDecksStatsRepository;
    @Mock
    private UserPublicDeckScoreRepository userPublicDeckScoreRepository;

    private DeckData mockedPrivateDeck;
    private DeckData mockedPublicDeck;
    private Set<CardData> mockedCards;
    private UserData mockedUser;

    private TrainService trainService;

    @BeforeEach
    void init() {
        trainService = new TrainService(deckRepository, userRepository, publicDecksStatsRepository, userPublicDeckScoreRepository);

        mockedPrivateDeck = new DeckData("Test deck", "Test deck description", false);
        mockedPrivateDeck.setId(17);

        Set<CardData> privateCards = new HashSet<>();
        privateCards.add(new CardData(0, "", "", "bebra", "", -10, mockedPrivateDeck));
        privateCards.add(new CardData(1, "", "", "bebra", "", 1, mockedPrivateDeck));
        privateCards.add(new CardData(2, "", "", "bebra", "", 1, mockedPrivateDeck));
        privateCards.add(new CardData(3, "", "", "bebra", "", 1, mockedPrivateDeck));
        privateCards.add(new CardData(4, "", "", "bebra", "", 1, mockedPrivateDeck));
        mockedCards = privateCards;
        mockedPrivateDeck.setCards(privateCards);

        mockedPublicDeck = new DeckData("Test deck", "Test deck description", true);
        mockedPublicDeck.setId(18);

        Set<CardData> pubCards = new HashSet<>();
        pubCards.add(new CardData(5, "", "", "bebra", "", -10, mockedPublicDeck));
        pubCards.add(new CardData(6, "", "", "bebra", "", 1, mockedPublicDeck));
        pubCards.add(new CardData(7, "", "", "bebra", "", 1, mockedPublicDeck));
        pubCards.add(new CardData(8, "", "", "bebra", "", 1, mockedPublicDeck));
        pubCards.add(new CardData(9, "", "", "bebra", "", 1, mockedPublicDeck));
        mockedPublicDeck.setCards(pubCards);

        mockedUser = new UserData("Rostik", "Parol", "token");
        mockedUser.setId(1912);

        Set<UserDeckData> userDeckData = new HashSet<>();
        userDeckData.add(new UserDeckData(0, mockedUser, mockedPrivateDeck));
        userDeckData.add(new UserDeckData(1, mockedUser, mockedPublicDeck));
        mockedPrivateDeck.setUsers(userDeckData);
        mockedPublicDeck.setUsers(userDeckData);
        mockedUser.setDecks(userDeckData);

        Mockito.when(userRepository.findByUserName("Rostik")).thenReturn(Optional.of(mockedUser));
        Mockito.when(deckRepository.findById(17)).thenReturn(Optional.of(mockedPrivateDeck));
        Mockito.when(deckRepository.findById(18)).thenReturn(Optional.of(mockedPublicDeck));

    }

    @Test
    @DisplayName("TrainServiceTest:testAddPointsWithAnki()")
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    public void testAddPointsWithAnki() {
        var mockedUserDeckData = new UserDeckData();
        mockedUserDeckData.setId(0);
        mockedUserDeckData.setDeck(mockedPrivateDeck);
        mockedUserDeckData.setUser(mockedUser);

        Mockito.when(deckRepository.findByUserId(1912)).thenReturn(Optional.of(Set.of(mockedUserDeckData)));
        Mockito.when(publicDecksStatsRepository.findById(18)).thenReturn(Optional.of(new PublicDeckStats()));

        var onEasyAdded = trainService.addPointsWithAnki(new PointsAnkiDto(17, 1, Difficulty.EASY));
        var onGoodAdded = trainService.addPointsWithAnki(new PointsAnkiDto(17, 2, Difficulty.GOOD));
        var onHardAdded = trainService.addPointsWithAnki(new PointsAnkiDto(17, 3, Difficulty.HARD));
        var onFailAdded = trainService.addPointsWithAnki(new PointsAnkiDto(17, 4, Difficulty.FAIL));

        Assertions.assertEquals(2, onEasyAdded);
        Assertions.assertEquals(1, onGoodAdded);
        Assertions.assertEquals(0, onHardAdded);
        Assertions.assertEquals(-1, onFailAdded);

        var onIllegalKnowledgePoints = trainService.addPointsWithAnki(new PointsAnkiDto(17, 0, Difficulty.GOOD));
        Assertions.assertEquals(1, onIllegalKnowledgePoints);

        Assertions.assertThrows(InvalidTrainingTypeException.class, () -> trainService.addPointsWithAnki(new PointsAnkiDto(18, 5, Difficulty.GOOD)));
    }

    @Test
    @DisplayName("TrainServiceTest:testAddPointsWithIntegral()")
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    public void testAddPointsWithIntegral() {

        var pointsIntegralDtoWithWrongCardId = new PointsIntegralDto(
                mockedPublicDeck.getId(),
                228,
                Difficulty.EASY
        );
        Assertions.assertThrows(
                NotFoundException.class,
                () -> trainService.addPointByIntegral(pointsIntegralDtoWithWrongCardId),
                "User doesn't have decks"
        );
        var mockedUserDeckDataPrivate = new UserDeckData();
        mockedUserDeckDataPrivate.setId(0);
        mockedUserDeckDataPrivate.setDeck(mockedPublicDeck);
        mockedUserDeckDataPrivate.setUser(mockedUser);
        var mockedUserDeckDataPublic = new UserDeckData();
        mockedUserDeckDataPublic.setId(0);
        mockedUserDeckDataPublic.setDeck(mockedPrivateDeck);
        mockedUserDeckDataPublic.setUser(mockedUser);
        Mockito.when(deckRepository.findByUserId(1912)).thenReturn(Optional.of(Set.of(mockedUserDeckDataPrivate, mockedUserDeckDataPublic)));
        Assertions.assertThrows(
                WrongDeckCardRelationException.class,
                () -> trainService.addPointByIntegral(pointsIntegralDtoWithWrongCardId),
                "Card with id 228 not found in deck with id 18"
        );
        var pointsIntegralDtoWithWrongDeckId = new PointsIntegralDto(
                228,
                0,
                Difficulty.EASY
        );
        Assertions.assertThrows(
                WrongDeckCardRelationException.class,
                () -> trainService.addPointByIntegral(pointsIntegralDtoWithWrongDeckId),
                "Card with id 0 not found in deck with id 228"
        );
        var pointsIntegralDtoWithPrivateDeckId = new PointsIntegralDto(
                mockedPrivateDeck.getId(),
                0,
                Difficulty.EASY
        );
        Assertions.assertThrows(
                InvalidTrainingTypeException.class,
                () -> trainService.addPointByIntegral(pointsIntegralDtoWithPrivateDeckId),
                "Integral training type is only available for public decks"
        );
        var pointsIntegralDto = new PointsIntegralDto(
                mockedPublicDeck.getId(),
                5,
                Difficulty.HARD
        );
        var result = trainService.addPointByIntegral(pointsIntegralDto);
        Assertions.assertEquals(0, result);

        var userScore = new UserPublicDeckScoreData(
                (long) mockedUser.getId(),
                (long) mockedPublicDeck.getId(),
                Double.NaN
        );
        Mockito.when(userPublicDeckScoreRepository
                        .findByUserIdAndDeckId((long) mockedUser.getId(), (long) mockedPublicDeck.getId()))
                .thenReturn(Optional.of(userScore));
        Mockito.when(publicDecksStatsRepository.findById(18)).thenReturn(Optional.of(new PublicDeckStats()));
        pointsIntegralDto = new PointsIntegralDto(
                mockedPublicDeck.getId(),
                5,
                Difficulty.FAIL
        );
        result = trainService.addPointByIntegral(pointsIntegralDto);
        Assertions.assertEquals(0.0, result);
        var userScoreLow = new UserPublicDeckScoreData(
                (long) mockedUser.getId(),
                (long) mockedPublicDeck.getId(),
                -1000.0
        );
        Mockito.when(userPublicDeckScoreRepository
                        .findByUserIdAndDeckId((long) mockedUser.getId(), (long) mockedPublicDeck.getId()))
                .thenReturn(Optional.of(userScoreLow));
        pointsIntegralDto = new PointsIntegralDto(
                mockedPublicDeck.getId(),
                5,
                Difficulty.GOOD
        );
        result = trainService.addPointByIntegral(pointsIntegralDto);
        Assertions.assertEquals(0.0, result);
        pointsIntegralDto = new PointsIntegralDto(
                mockedPublicDeck.getId(),
                5,
                Difficulty.EASY
        );
        result = trainService.addPointByIntegral(pointsIntegralDto);
        Assertions.assertEquals(0.0, result);
    }

    @Test
    @DisplayName("TrainServiceTest:testAddPointsByTime()")
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    public void testAddPointsByTime() {
        var trainTimeDtoPublicDeck = new TrainTimeDto(
                18,
                0,
                "bebra",
                1.0
        );
        var statsForPublic = new PublicDeckStats(
                18,
                10,
                20,
                40,
                80
        );
        Mockito.when(publicDecksStatsRepository.findById(18)).thenReturn(Optional.of(statsForPublic));
        Assertions.assertThrows(
                InvalidTrainingTypeException.class,
                () -> trainService.addPointsByTime(trainTimeDtoPublicDeck),
                "Only integral system available for public decks"
        );

        var trainTimeDtoFastAnswer = new TrainTimeDto(
                17,
                0,
                "bebra",
                1.0
        );
        var trainTimeDtoAverageAnswer = new TrainTimeDto(
                17,
                0,
                "bebra",
                4.0
        );
        var trainTimeDtoSlowAnswer = new TrainTimeDto(
                17,
                0,
                "bebra",
                7.0
        );
        var trainTimeDtoVerySlowAnswer = new TrainTimeDto(
                17,
                0,
                "bebra",
                12.0
        );
        var trainTimeDtoWrongCard = new TrainTimeDto(
                17,
                228,
                "bebra",
                12.0
        );
        Assertions.assertThrows(
                NotFoundException.class,
                () -> trainService.addPointsByTime(trainTimeDtoFastAnswer),
                "User doesn't have decks"
        );

        var mockedUserDeckData = new UserDeckData();
        mockedUserDeckData.setId(0);
        mockedUserDeckData.setDeck(mockedPrivateDeck);
        mockedUserDeckData.setUser(mockedUser);
        Mockito.when(deckRepository.findByUserId(1912)).thenReturn(Optional.of(Set.of(mockedUserDeckData)));
        Mockito.when(publicDecksStatsRepository.findById(17)).thenReturn(Optional.empty());
        Mockito.when(publicDecksStatsRepository.findById(18)).thenReturn(Optional.of(new PublicDeckStats()));
        Assertions.assertThrows(
                WrongDeckCardRelationException.class,
                () -> trainService.addPointsByTime(trainTimeDtoWrongCard),
                "Card with id 228 not found in deck with id 17"
        );
        Assertions.assertEquals(
                2,
                trainService.addPointsByTime(trainTimeDtoFastAnswer)
        );
        Assertions.assertEquals(
                1,
                trainService.addPointsByTime(trainTimeDtoAverageAnswer)
        );
        Assertions.assertEquals(
                0,
                trainService.addPointsByTime(trainTimeDtoSlowAnswer)
        );
        Assertions.assertEquals(
                -1,
                trainService.addPointsByTime(trainTimeDtoVerySlowAnswer)
        );

    }

    @Test
    @DisplayName("TrainServiceTest:testMapDifficultyToPointByStats()")
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    public void testMapDifficultyToPointByStats() {
        var stats = new PublicDeckStats(
                18,
                10,
                20,
                40,
                80
        );
        double eps = 0.001;
        int totalAnswers = stats.getEasyCount() + stats.getGoodCount() + stats.getHardCount() + stats.getFailCount();
        double part = ((double) stats.getFailCount()) / totalAnswers + eps;
        double coefficient = 1 / (part);
        Assertions.assertEquals(
                (coefficient * -1) % 10.0,
                trainService.mapDifficultyToPointByStats(Difficulty.FAIL, stats)
        );
        Assertions.assertEquals(
                0.0,
                trainService.mapDifficultyToPointByStats(Difficulty.HARD, stats)
        );
        double betterAnswersPart = ((double) stats.getEasyCount()) / totalAnswers + eps;
//        double coefficient;
        if (betterAnswersPart > 0.5) {
            coefficient = 1 - betterAnswersPart;
        } else {
            coefficient = 1 / betterAnswersPart;
        }
        Assertions.assertEquals(
                coefficient % 10.0,
                trainService.mapDifficultyToPointByStats(Difficulty.GOOD, stats)
        );
        var stats2 = new PublicDeckStats(
                18,
                10,
                20,
                40,
                80
        );
        betterAnswersPart = ((double) stats2.getEasyCount()) / totalAnswers + eps;
//        double coefficient;
        if (betterAnswersPart > 0.5) {
            coefficient = 1 - betterAnswersPart;
        } else {
            coefficient = 1 / betterAnswersPart;
        }
        Assertions.assertEquals(
                coefficient % 10.0,
                trainService.mapDifficultyToPointByStats(Difficulty.GOOD, stats2)
        );
        part = ((double) stats.getEasyCount()) / totalAnswers + eps;
        coefficient = 1 / (part);
        Assertions.assertEquals(
                (coefficient * 2) % 10.0,
                trainService.mapDifficultyToPointByStats(Difficulty.EASY, stats)
        );
    }

    @Test
    @DisplayName("TrainServiceTest:testIsPublic()")
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    public void testIsPublic() {
        var stats = new PublicDeckStats(
                18,
                10,
                20,
                40,
                80
        );
        Mockito.when(publicDecksStatsRepository.findById(17)).thenReturn(Optional.empty());
        Mockito.when(publicDecksStatsRepository.findById(18)).thenReturn(Optional.of(stats));
        Assertions.assertTrue(trainService.isPublic(18));
        Assertions.assertFalse(trainService.isPublic(17));
    }
}
