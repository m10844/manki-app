package ru.nsu.ccfit.mankiapp.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import ru.nsu.ccfit.mankiapp.MankiappApplication;
import ru.nsu.ccfit.mankiapp.api.data.request.CardAddRequest;
import ru.nsu.ccfit.mankiapp.api.data.request.CardDeleteRequest;
import ru.nsu.ccfit.mankiapp.api.data.request.CardEditRequest;
import ru.nsu.ccfit.mankiapp.api.data.request.DeckAddRequest;
import ru.nsu.ccfit.mankiapp.api.data.request.DeckEditRequest;
import ru.nsu.ccfit.mankiapp.api.data.response.*;
import ru.nsu.ccfit.mankiapp.api.data.security.UserCredentialsRequest;
import ru.nsu.ccfit.mankiapp.dto.CardDto;
import ru.nsu.ccfit.mankiapp.dto.DeckDto;
import ru.nsu.ccfit.mankiapp.dto.DeckEditDto;
import ru.nsu.ccfit.mankiapp.services.DeckService;

import java.util.ArrayList;
import java.util.Collections;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {MankiappApplication.class})
class DeckControllerTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    ObjectMapper mapper;

    private MockMvc mockMvc;

    private String token;

    @Mock
    private DeckController deckController;

    @BeforeEach
    void initMock() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();

        var loginRequest = new UserCredentialsRequest("Andrew", "adminadmin");

        var res = mockMvc.perform(post("/auth/login").contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(loginRequest)))
                .andExpect(status().isOk()).andReturn();
        token = JsonPath.parse(res.getResponse().getContentAsString()).read("$.token").toString();
        System.out.println(token);
    }

    @Test
    void testGetDeck() throws Exception {
        mockMvc.perform(get("/api/deck/get/101").contentType(MediaType.APPLICATION_JSON).header("Authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deck.id").value("101"));
    }

    @Test
    void testEditDeck() throws Exception {
        var request = new DeckEditDto(101, "Test deck", "New description");

        mockMvc.perform(post("/api/deck/edit").contentType(MediaType.APPLICATION_JSON).header("Authorization", "Bearer " + token)
                        .content(mapper.writeValueAsString(request))
                )
                .andExpect(status().isOk());
    }

//    @Test
//    void testEditCard() {
//        Mockito.when(deckController.addDeck(Mockito.any())).thenReturn(new DeckResponse(new DeckDto(1, "a", "a", false, Collections.emptyList())));
//        Mockito.when(deckController.addCard(Mockito.any())).thenReturn(new CardResponse(new CardDto(1, "a", "a", "a", "a", 1)));
//        var r = (DeckResponse) deckController.addDeck(new DeckAddRequest("Title", "Descr"));
//        var r2 = (CardResponse) deckController.addCard(new CardAddRequest("a", "a", "a", "a", r.deck().id()));
//        var r3 = (DeckResponse) deckController.editCard(new CardEditRequest(r.deck().id(), new CardDto(r2.card().id(), "title", "/", "descr", "/", 1)));
//        var l = new ArrayList<CardDto>();
//        l.add(new CardDto(1, "title", "/", "descr", "/", 1));
//        Mockito.when(deckController.editDeck(Mockito.any())).thenReturn(new DeckResponse(new DeckDto(1, "title", "descr", false, l)));
//        assertEquals("title", r3.deck().cards().stream().findFirst().orElseThrow().question());
//    }

    @Test
    void testDeckEdit() {
        Mockito.when(deckController.editDeck(Mockito.any())).thenReturn(new DeckResponse(new DeckDto(1, "title", "descr", false, Collections.emptyList())));
        var response = (DeckResponse) deckController.editDeck(new DeckEditRequest(1, "title", "old"));
        assertEquals("title", response.deck().deckTitle());
    }

    @Test
    void testAddCard() {
        Mockito.when(deckController.addCard(Mockito.any())).thenReturn(new CardResponse(new CardDto(1, "a", "a", "a", "a", 1)));
        var response = (CardResponse) deckController.addCard(new CardAddRequest("a", "a", "a", "a", 1));
        assertEquals("a", response.card().answer());
        assertEquals("a", response.card().answerImagePath());
        assertEquals("a", response.card().question());
        assertEquals("a", response.card().questionImagePath());
        assertEquals(1, response.card().id());
    }

    @Test
    void testDeleteCards() {
        Mockito.when(deckController.deleteCards(Mockito.any())).thenReturn(new DeckResponse(new DeckDto(1, "a", "a", false, Collections.emptyList())));
        var response = (DeckResponse) deckController.deleteCards(new CardDeleteRequest(1, Collections.emptyList()));
        assertEquals(response.deck().deckTitle(), "a");
        assertEquals(response.deck().deckDescription(), "a");
        assertEquals(response.deck().id(), 1);
        assertEquals(response.deck().isPublic(), false);
    }

    @Test
    void testAddDeck() {
        Mockito.when(deckController.addDeck(Mockito.any())).thenReturn(new DeckResponse(new DeckDto(1, "a", "a", false, Collections.emptyList())));
        var response = (DeckResponse) deckController.addDeck(new DeckAddRequest("a", "a"));
        assertEquals(response.deck().deckTitle(), "a");
        assertEquals(response.deck().deckDescription(), "a");
        assertEquals(response.deck().id(), 1);
        assertEquals(response.deck().isPublic(), false);
    }

    @Test
    void testGetAllDecks() {
        Mockito.when(deckController.getAllDecks()).thenReturn(new AllDecksResponse(Collections.emptyList()));
        var response = (AllDecksResponse) deckController.getAllDecks();
        assertEquals(response.deckInfo(), Collections.emptyList());
    }

    @Test
    void testShareDeck() {
        Mockito.when(deckController.shareDeck(1)).thenReturn(new SharedDeckResponse(1, "a"));
        var response = (SharedDeckResponse) deckController.shareDeck(1);
        assertEquals(response.deckId(), 1);
        assertEquals(response.shareToken(), "a");
    }

    @Test
    void testGetSharedDeck() {
        Mockito.when(deckController.getSharedDeck("a")).thenReturn(new DeckResponse(new DeckDto(1, "a", "a", false, Collections.emptyList())));
        var response = (DeckResponse) deckController.getSharedDeck("a");
        assertEquals(response.deck().deckTitle(), "a");
        assertEquals(response.deck().deckDescription(), "a");
        assertEquals(response.deck().id(), 1);
        assertEquals(response.deck().isPublic(), false);
    }

    @Test
    void testMakePublic() {
        Mockito.when(deckController.makePublic(1)).thenReturn(new PublicDeckResponse(1, true));
        var response = (PublicDeckResponse) deckController.makePublic(1);
        assertEquals(response.deckId(), 1);
        assertEquals(response.isSuccess(), true);
    }

    @Test
    void testGetAllPublic() {
        Mockito.when(deckController.getAllPublic()).thenReturn(new AllDecksResponse(Collections.emptyList()));
        var response = (AllDecksResponse) deckController.getAllPublic();
        assertEquals(response.deckInfo(), Collections.emptyList());
    }

}