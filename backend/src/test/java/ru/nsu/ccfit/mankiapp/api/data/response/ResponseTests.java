package ru.nsu.ccfit.mankiapp.api.data.response;

import org.junit.jupiter.api.Test;

import ru.nsu.ccfit.mankiapp.api.data.request.ChangePasswordRequest;
import ru.nsu.ccfit.mankiapp.dto.DeckInfoDto;

public class ResponseTests {
    @Test
    void deckInfoResponse() {
        var e = new DeckInfoResponse(new DeckInfoDto(1, "", "", true, 1));
    }

    @Test
    void errorResponse() {
        var e = new ErrorResponse(1, "");
    }
    @Test
    void changePasswordRequest() {
        var e = new ChangePasswordResponse("");
    }
}
