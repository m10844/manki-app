package ru.nsu.ccfit.mankiapp.model.data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

public class UserDataTests {
    @Test
    void testNotEquals() {
        var u1 = new UserData(0, "User1", "Pass1", "Token1", new HashSet<>());
        var u2 = new UserData(1, "User2", "Pass2", "Token2", new HashSet<>());
        Assertions.assertNotEquals(u1, u2);
    }

    @Test
    void testEquals() {
        var u1 = new UserData("User1", "Pass1", "Token1");
        var u2 = new UserData("User1", "Pass1", "Token1");
        Assertions.assertEquals(u1, u2);
    }

    @Test
    void hashCodeTest() {
        var u = new UserData("User1", "Pass1", "Token1");
        Assertions.assertEquals(0, u.hashCode());
    }
}
