package ru.nsu.ccfit.mankiapp.services;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import ru.nsu.ccfit.mankiapp.dto.*;
import ru.nsu.ccfit.mankiapp.dto.mapper.DeckMapper;
import ru.nsu.ccfit.mankiapp.exceptions.BadRequestException;
import ru.nsu.ccfit.mankiapp.exceptions.NotFoundException;
import ru.nsu.ccfit.mankiapp.exceptions.WrongFieldLengthException;
import ru.nsu.ccfit.mankiapp.exceptions.WrongIdentifierException;
import ru.nsu.ccfit.mankiapp.model.data.*;
import ru.nsu.ccfit.mankiapp.model.repo.*;
import ru.nsu.ccfit.mankiapp.model.repo.security.UserRepository;

import java.util.*;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@MockitoSettings(strictness = Strictness.LENIENT)
class DeckServiceTest {

    @Mock private DeckRepository deckRepository;
    @Mock private CardRepository cardRepository;
    @Mock private UserRepository userRepository;
    @Mock private UserDeckRepository userDeckRepository;
    @Mock private SharedDeckRepository sharedDeckRepository;
    @Mock private PublicDecksStatsRepository publicDecksStatsRepository;


    private DeckService deckService;
    private DeckData mockedDeck;

    private Set<CardData> mockedCards;

    private UserData mockedUser;

    @BeforeEach
    void init() {
        deckService = new DeckService(
                deckRepository,
                cardRepository,
                userRepository,
                userDeckRepository,
                sharedDeckRepository,
                publicDecksStatsRepository
        );
        mockedDeck = new DeckData("Test deck", "Test deck description", false);
        mockedDeck.setId(17);

        Set<CardData> cards = new HashSet<>();
        cards.add(new CardData(0, "", "", "", "", 1, mockedDeck));
        cards.add(new CardData(1, "", "", "", "", 1, mockedDeck));
        cards.add(new CardData(2, "", "", "", "", 1, mockedDeck));
        cards.add(new CardData(3, "", "", "", "", 1, mockedDeck));
        cards.add(new CardData(4, "", "", "", "", 1, mockedDeck));
        mockedCards = cards;
        mockedDeck.setCards(cards);

        mockedUser = new UserData("Rostik", "Parol", "token");
        mockedUser.setId(1912);
        Set<UserDeckData> userDeckData = new HashSet<>();
        userDeckData.add(new UserDeckData(0, mockedUser, mockedDeck));
        mockedDeck.setUsers(userDeckData);
        mockedUser.setDecks(userDeckData);

        Mockito.when(userRepository.findByUserName("Rostik")).thenReturn(Optional.of(mockedUser));
        Mockito.when(deckRepository.findById(17)).thenReturn(Optional.of(mockedDeck));
    }

    @Test
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    void getDeck() {
        Mockito.when(cardRepository.findAllByDeckId(17)).thenReturn(mockedDeck.getCards().stream().toList());

        DeckDto deck = deckService.getDeck(17);

        Assertions.assertEquals(deck.deckTitle(), mockedDeck.getDeckTitle());
        Assertions.assertEquals(deck.deckDescription(), mockedDeck.getDeckDescription());
//        Assertions.assertEquals(
//                deck.getCards().stream().map(DeckMapper::dataFromCard).toList(),
//                mockedDeck.getCards().stream().toList()
//        );
        Assertions.assertThrows(
                WrongIdentifierException.class,
                () -> deckService.getDeck(1337),
                "Deck with given identifier does not exist"
        );
    }

    @Test
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    void modifyCards() {
        Mockito.when(cardRepository.findAllByDeckId(17)).thenReturn(mockedDeck.getCards().stream().toList());

        Mockito.when(cardRepository.findById(1))
                .thenReturn(Optional.of(mockedCards.stream().filter(e -> e.getId() == 1).findFirst().orElseThrow()));

        CardDto cardToModify = new CardDto(1,
                "Changed 1",
                "Changed 1",
                "Changed 1",
                "Changed 1",
                0);
        CardWithDeckIdDto modObj = new CardWithDeckIdDto(cardToModify, 17);
        DeckDto deckWithModifiedCard = deckService.modifyCards(modObj);

        Assertions.assertEquals(
                deckWithModifiedCard.cards().stream().filter(e -> e.id() == 1).findFirst().orElseThrow(),
                cardToModify
        );

        CardDto wrongCardToModify = new CardDto(100,
                "Changed 1",
                "Changed 1",
                "Changed 1",
                "Changed 1",
                0);
        Assertions.assertThrows(
                WrongIdentifierException.class,
                () -> deckService.modifyCards(new CardWithDeckIdDto(wrongCardToModify, 100)),
                "Deck with given identifier does not exist"
        );
        Assertions.assertThrows(
                WrongIdentifierException.class,
                () -> deckService.modifyCards(new CardWithDeckIdDto(wrongCardToModify, 17)),
                "Card with id 100 does not exist"
        );
    }

    @Test
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    void addCard() {
//        Mockito.when(cardRepository.findById(1234))
//                .thenReturn(Optional.of(mockedCards.stream().filter(e -> e.getId() == 1234).findFirst().orElseThrow()));
        Mockito.when(cardRepository.findAllByDeckId(17)).thenReturn(mockedDeck.getCards().stream().toList());

        CardAddingDto cardToAdd = new CardAddingDto(
                "New 1",
                "New 1",
                "New 1",
                "New 1",
                0,
                17);
        Mockito.when(cardRepository.save(DeckMapper.dataToAddFromCard(cardToAdd)))
                .thenReturn(DeckMapper.dataToAddFromCard(cardToAdd));
//        CardWithDeckIdDto modObj = new CardWithDeckIdDto(cardToModify, 17);
        CardDto newCard = deckService.addCard(cardToAdd);


        Assertions.assertEquals(newCard.answer(),cardToAdd.answer());
        Assertions.assertEquals(newCard.answerImagePath(),cardToAdd.answerImagePath());
        Assertions.assertEquals(newCard.question(),cardToAdd.question());
        Assertions.assertEquals(newCard.questionImagePath(),cardToAdd.questionImagePath());
        Assertions.assertEquals(newCard.knowledgePoints(),cardToAdd.knowledgePoints());

        CardAddingDto wrongCardToAdd = new CardAddingDto(
                "New 1",
                "New 1",
                "New 1",
                "New 1",
                0,
                100);
        Assertions.assertThrows(
                WrongIdentifierException.class,
                () -> deckService.addCard(wrongCardToAdd),
                "Deck with given identifier does not exist"
        );
    }

    @Test
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    void deleteCards() {
        var cardsIds = List.of(1,2,3);
        var cardsToDelete = new CardsIdsWithDeckIdDto(
          cardsIds,
          17
        );
        var deletedCards = mockedCards.stream()
                .filter(c ->cardsIds.contains(c.getId())).toList();
        var leftCards = mockedCards.stream().filter((c) -> !deletedCards.contains(c)).toList();
        Mockito.when(cardRepository.findAllById(cardsIds)).thenReturn(deletedCards);
        Mockito.when(cardRepository.findAllByDeckId(17))
                .thenReturn(
                        mockedDeck.getCards().stream()
                                .filter((c) -> !deletedCards.contains(c)).toList()
        );
        var deletionResult = deckService.deleteCards(cardsToDelete);
        Assertions.assertEquals(
                deletionResult.cards().stream().sorted(Comparator.comparingInt(CardDto::id)).toList(),
                leftCards.stream().map(DeckMapper::cardFromData).toList()
        );
        var wrongCardsToDelete = new CardsIdsWithDeckIdDto(
                cardsIds,
                177
        );
        Assertions.assertThrows(
                WrongIdentifierException.class,
                () -> deckService.deleteCards(wrongCardsToDelete),
                "Deck with given identifier does not exist"
        );
    }

    @Test
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    void addDeck() {
        String tooLongString = "ARARARARARARARARARARARARA" +
                "RARRARARARARAARARARARARAR" +
                "ARARARARARARARARRARARARAR" +
                "AARARARARARARARARARARARAR" +
                "ARARRARARARARAARARARARARA" +
                "RARARARARARARARARRARARARA" +
                "RARARARARARARARARRARARARA" +
                "RARARARARARARARARRARARARA" +
                "RARARARARARARARARRARARARA" +
                "RARARARARARARARARRARARARA" +
                "RARARARARARARARARRARARARA";
        var deckToAdd = new DeckDto(
                27,
                "New deck title",
                "New deck description",
                false,
                Collections.emptyList()
        );
        var addDeckData = DeckMapper.dataFromDeck(deckToAdd, false);
        var addDeckDataWithId = DeckMapper.dataFromDeck(deckToAdd, false);
        addDeckDataWithId.setId(27);
        Mockito.when(deckRepository.save(addDeckData))
                .thenReturn(addDeckDataWithId);
        var addedDeck = deckService.addDeck(deckToAdd);
        Assertions.assertEquals(addedDeck, deckToAdd);

        var deckToAddWithLongTitle = new DeckDto(
                27,
                "New deck title" + tooLongString,
                "New deck description",
                false,
                Collections.emptyList()
        );
        Assertions.assertThrows(
                WrongFieldLengthException.class,
                () -> deckService.addDeck(deckToAddWithLongTitle),
                "Deck title can not be longer than 255 characters"
        );
        var deckToAddWithLongDescription = new DeckDto(
                27,
                "New deck title",
                "New deck description" + tooLongString,
                false,
                Collections.emptyList()
        );
        Assertions.assertThrows(
                WrongFieldLengthException.class,
                () -> deckService.addDeck(deckToAddWithLongDescription),
                "Deck description can not be longer than 255 characters"
        );
    }

    @Test
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    void deleteDeck() {
        Assertions.assertThrows(
                WrongIdentifierException.class,
                () -> deckService.deleteDeck(27),
                "Deck with given identifier does not exist"
        );
    }

    @Test
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    void getAllDecks() {
        Mockito.when(userRepository.findById(1912))
                .thenReturn(Optional.of(mockedUser));
        DeckInfoDto rightDeck = new DeckInfoDto(
                mockedDeck.getId(),
                mockedDeck.getDeckTitle(),
                mockedDeck.getDeckDescription(),
                mockedDeck.getIsPublic(),
                mockedDeck.getCards().size()
        );
        Assertions.assertEquals(
                deckService.getAllDecks("Rostik"),
                List.of(rightDeck)
        );
    }

    @Test
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    void editDeck() {
        String tooLongString = "ARARARARARARARARARARARARA" +
                "RARRARARARARAARARARARARAR" +
                "ARARARARARARARARRARARARAR" +
                "AARARARARARARARARARARARAR" +
                "ARARRARARARARAARARARARARA" +
                "RARARARARARARARARRARARARA" +
                "RARARARARARARARARRARARARA" +
                "RARARARARARARARARRARARARA" +
                "RARARARARARARARARRARARARA" +
                "RARARARARARARARARRARARARA" +
                "RARARARARARARARARRARARARA";
        var deckToEdit = new DeckEditDto(
                17,
                "EDITED TITLE",
                "EDITED DESCRIPTION"
        );
        DeckInfoDto rightResult = new DeckInfoDto(
                17,
                "EDITED TITLE",
                "EDITED DESCRIPTION",
                mockedDeck.getIsPublic(),
                mockedDeck.getCards().size()
        );
        var result = deckService.editDeck(deckToEdit);
        Assertions.assertEquals(
                result,
                rightResult
        );
        var deckToEditWithLongTitle = new DeckEditDto(
                17,
                "EDITED TITLE" + tooLongString,
                "EDITED DESCRIPTION"
        );
        Assertions.assertThrows(
                WrongFieldLengthException.class,
                () -> deckService.editDeck(deckToEditWithLongTitle),
                "Too long string for deck title"
        );
        var deckToEditWithLongDescription = new DeckEditDto(
                17,
                "EDITED TITLE",
                "EDITED DESCRIPTION" + tooLongString
        );
        Assertions.assertThrows(
                WrongFieldLengthException.class,
                () -> deckService.editDeck(deckToEditWithLongDescription),
                "Too long string for deck description"
        );
    }

    @Test
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    void shareDeck() {
        var rightSharedDeckData = new SharedDeckData(
               17,
               "SHARE TOKEN"
        );
        Mockito.when(sharedDeckRepository
                        .save(Mockito.any()))
                .thenReturn(rightSharedDeckData);
        var result = deckService.shareDeck(17);
        Assertions.assertEquals(
                result,
                DeckMapper.sharedDeckDataToDto(rightSharedDeckData)
        );
    }

    @Test
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    void getSharedDeck() {
        var rightSharedDeckData = new SharedDeckData(
                17,
                "SHARE TOKEN"
        );
        Mockito.when(sharedDeckRepository.findSharedDeckDataByShareToken("SHARE TOKEN"))
                .thenReturn(Optional.of(rightSharedDeckData));
        Assertions.assertThrows(
                BadRequestException.class,
                () -> deckService.getSharedDeck("SHARE TOKEN"),
                "You already have this deck"
        );
        mockedDeck.setUsers(new HashSet<>());
        mockedUser.setDecks(new HashSet<>());

        Assertions.assertEquals(
                DeckMapper.deckFromData(mockedDeck).deckTitle(),
                deckService.getSharedDeck("SHARE TOKEN").deckTitle()
        );
        Assertions.assertEquals(
                DeckMapper.deckFromData(mockedDeck).deckDescription(),
                deckService.getSharedDeck("SHARE TOKEN").deckDescription()
        );
        Assertions.assertThrows(
                NotFoundException.class,
                () -> deckService.getSharedDeck("WRONG TOKEN"),
                "Wrong share token"
        );
        var sharedDeckDataWithWrongDeckId = new SharedDeckData(
                27,
                "SHARE TOKEN"
        );
        Mockito.when(sharedDeckRepository.findSharedDeckDataByShareToken("SHARE TOKEN"))
                .thenReturn(Optional.of(sharedDeckDataWithWrongDeckId));
        Assertions.assertThrows(
                NotFoundException.class,
                () -> deckService.getSharedDeck("SHARE TOKEN"),
                "Deck not found"
        );
    }

    @Test
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    void makeDeckPublic() {
        boolean result = deckService.makeDeckPublic(17);
        Assertions.assertTrue(result);
        mockedDeck.setIsPublic(true);
        Assertions.assertFalse(deckService.makeDeckPublic(17));
        Assertions.assertThrows(
                NotFoundException.class,
                () -> deckService.makeDeckPublic(27),
                "Deck with id 27 not found"
        );
    }

    @Test
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    void getAllPublic() {
        Mockito.when(deckRepository.findDeckDataByIsPublic(true)).thenReturn(
                Optional.of(List.of(mockedDeck))
        );
        var rightDto = DeckMapper.deckInfoFromData(mockedDeck);
        Assertions.assertEquals(
                List.of(rightDto),
                deckService.getAllPublic()
        );
    }
}