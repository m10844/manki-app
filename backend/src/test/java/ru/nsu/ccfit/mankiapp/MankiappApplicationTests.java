package ru.nsu.ccfit.mankiapp;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MankiappApplicationTests {
	volatile int valA = 1;
	volatile int valB = 1;
	@Test
	void contextLoads() {
		assertEquals(valA, valB);
	}
}
