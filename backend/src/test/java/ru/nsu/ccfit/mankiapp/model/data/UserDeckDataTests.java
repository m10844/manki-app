package ru.nsu.ccfit.mankiapp.model.data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UserDeckDataTests {
    @Test
    void testToString() {
        var a = new UserDeckData(0, new UserData(), new DeckData());
        Assertions.assertEquals(0, a.id);
        Assertions.assertNotNull(a.toString());
        var b = new PublicDeckStats();
        Assertions.assertNull(b.getDeckId());
        Assertions.assertEquals(new DeckData().hashCode(), 0);
        Assertions.assertNotNull(new CardData().toString());
        Assertions.assertNotNull(new CardData(0, new DeckData()));
    }
}
