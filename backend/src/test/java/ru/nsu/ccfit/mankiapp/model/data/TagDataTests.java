package ru.nsu.ccfit.mankiapp.model.data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

public class TagDataTests {
    @Test
    void testNotEquals() {
        var u1 = new TagData(0, "Tag1");
        var u2 = new TagData(1, "Tag2");
        Assertions.assertNotEquals(u1, u2);
    }

    @Test
    void testEquals() {
        var u1 = new TagData("Tag1");
        var u2 = new TagData("Tag1");
        Assertions.assertEquals(u1, u2);
        Assertions.assertEquals(u1.getTagTitle(), "Tag1");
        Assertions.assertNull(u1.getId());
        Assertions.assertNotNull(u1.toString());
        u1.setId(0);
        u1.setTagTitle("NewTag");
        Assertions.assertEquals(u1.getTagTitle(), "NewTag");
        Assertions.assertEquals(u1.getId(), 0);
    }

    @Test
    void hashCodeTest() {
        var u = new TagData("Tag");
        Assertions.assertEquals(0, u.hashCode());
    }
}
