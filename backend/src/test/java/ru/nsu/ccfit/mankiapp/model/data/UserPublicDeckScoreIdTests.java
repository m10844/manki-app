package ru.nsu.ccfit.mankiapp.model.data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UserPublicDeckScoreIdTests {
    @Test
    void test() {
        var a = new UserPublicDeckScoreId(0L, 0L);
        var b = new UserPublicDeckScoreId();
        Assertions.assertNotEquals(a, b);
    }

    @Test
    void test2() {
        var a = new UserPublicDeckScoreData(0L, 0L, 0.);
        var b = new UserPublicDeckScoreData();
        var t = new SharedDeckData();
        Assertions.assertNotEquals(a.getDeckId(), b.getDeckId());
        Assertions.assertNotEquals(a.getUserId(), b.getUserId());
        Assertions.assertNotEquals(a, b);
    }
}
