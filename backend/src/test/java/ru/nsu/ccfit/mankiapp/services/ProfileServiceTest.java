package ru.nsu.ccfit.mankiapp.services;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import ru.nsu.ccfit.mankiapp.model.data.UserData;
import ru.nsu.ccfit.mankiapp.model.repo.security.UserRepository;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class ProfileServiceTest {
    @Mock
    private UserRepository userRepository;

    private ProfileService profileService;

    @BeforeEach
    void initProfileService() {
        profileService = new ProfileService(userRepository);
    }

    @Test
    @DisplayName("ProfileServiceTest:getProfileInfo()")
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    void getProfileInfo() {
        UserData mockedUser = new UserData("Rostik", "Parol", "token");
        Mockito.when(userRepository.findByUserName("Rostik")).thenReturn(Optional.of(mockedUser));

        var profile = profileService.getProfileInfo();

        Assertions.assertEquals(profile.name(), "Rostik");
        Assertions.assertEquals(profile.totalDecks(), 0);
        Assertions.assertEquals(profile.averageKnowledgePoints(), 0);

        Mockito.verify(userRepository, Mockito.times(1)).findByUserName(Mockito.any());
    }

    @Test
    @DisplayName("ProfileServiceTest:updatePassword()")
    @WithMockUser(username = "Rostik", password = "Parol", roles = "USER")
    void updatePassword() {
        //TODO
//        UserData mockedUser = new UserData("Rostik", "Parol", "token");
//        mockedUser.setId(17);
//        Mockito.when(userRepository.findByUserName("Rostik")).thenReturn(Optional.of(mockedUser));
//        Mockito.when(userRepository.updatePassword(17, "NewParol")).then(
//                mockedUser.setUserPassword("NewParol")
//        );
//
//        profileService.updatePassword("Parol", "NewParol");
//
//        assertEquals(mockedUser.getUserPassword(), "NewParol");
//
//        Mockito.verify(userRepository, Mockito.times(1)).findByUserName(Mockito.any());
//        Mockito.verify(userRepository, Mockito.times(1)).updatePassword(Mockito.any(), Mockito.any());
    }
}