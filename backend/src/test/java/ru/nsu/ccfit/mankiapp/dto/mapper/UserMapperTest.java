package ru.nsu.ccfit.mankiapp.dto.mapper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import ru.nsu.ccfit.mankiapp.api.data.response.ProfileResponse;
import ru.nsu.ccfit.mankiapp.dto.ProfileDto;
import ru.nsu.ccfit.mankiapp.model.data.CardData;
import ru.nsu.ccfit.mankiapp.model.data.DeckData;
import ru.nsu.ccfit.mankiapp.model.data.UserData;
import ru.nsu.ccfit.mankiapp.model.data.UserDeckData;

import java.util.HashSet;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

class UserMapperTest {

    @Test
    void userDataToProfileDto() {
        var data = new UserData(
                "Test",
                "Test",
                "Test"
        );
        var rightResult = new ProfileDto(
                "Test",
                0,
                0.0
        );
        Assertions.assertEquals(
                rightResult,
                UserMapper.userDataToProfileDto(data)
        );
    }

    @Test
    void userDecksNotNull() {
        var set = new HashSet<UserDeckData>();
        var cards = new HashSet<CardData>();
        cards.add(new CardData(0, new DeckData()));
        var deckData = new DeckData(0, "Title", "Descr", false, cards, new HashSet<>());
        set.add(new UserDeckData(0, new UserData(), deckData));
        var u = new UserData(0, "Name", "Pass", "Token", set);
        Assertions.assertNotNull(UserMapper.userDataToProfileDto(u));
    }

    @Test
    void dtoToProfileResponse() {
        var dto = new ProfileDto(
                "Test",
                1,
                1.0
        );
        var rightResult = new ProfileResponse(
                "Test",
                1,
                1.0
        );
        Assertions.assertEquals(
                rightResult,
                UserMapper.dtoToProfileResponse(dto)
        );
    }
}