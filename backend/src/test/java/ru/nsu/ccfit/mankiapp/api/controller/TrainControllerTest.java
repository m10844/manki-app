package ru.nsu.ccfit.mankiapp.api.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.context.SpringBootTest;
import ru.nsu.ccfit.mankiapp.api.data.request.Difficulty;
import ru.nsu.ccfit.mankiapp.api.data.request.PointsRequest;
import ru.nsu.ccfit.mankiapp.api.data.request.TrainTimeRequest;
import ru.nsu.ccfit.mankiapp.api.data.response.PointsResponse;
import ru.nsu.ccfit.mankiapp.dto.mapper.TrainPointsMapper;
import ru.nsu.ccfit.mankiapp.services.TrainService;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@MockitoSettings(strictness = Strictness.LENIENT)
class TrainControllerTest {
    @Mock
    private TrainService trainService;
    private TrainController trainController;

    @BeforeEach
    void init() {
        trainController = new TrainController(trainService);
    }

    @Test
    void byAnki() {
        var request = new PointsRequest(
                1,
                2,
                Difficulty.HARD
        );
        var rightResult = new PointsResponse(
                1,
                2,
                0
        );
        Mockito.when(trainService.addPointsWithAnki(TrainPointsMapper.requestToAnkiDto(request))).thenReturn(0);
        Assertions.assertEquals(
                trainController.byAnki(request),
                rightResult
        );
    }

    @Test
    void byIntegral() {
        var request = new PointsRequest(
                1,
                2,
                Difficulty.HARD
        );
        var rightResult = new PointsResponse(
                1,
                2,
                0
        );
        Mockito.when(trainService.addPointByIntegral(TrainPointsMapper.requestToIntegralDto(request))).thenReturn(0.0);
        Assertions.assertEquals(
                trainController.byIntegral(request),
                rightResult
        );
    }

    @Test
    void byTime() {
        var request = new TrainTimeRequest(
                1,
                2,
                "bebra", 6.0
        );
        var rightResult = new PointsResponse(
                1,
                2,
                0
        );
        Mockito.when(trainService.addPointsByTime(TrainPointsMapper.requestTimeToDto(request))).thenReturn(0);
        Assertions.assertEquals(
                trainController.byTime(request),
                rightResult
        );
    }
}