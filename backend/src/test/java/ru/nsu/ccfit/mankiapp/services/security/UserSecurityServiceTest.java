package ru.nsu.ccfit.mankiapp.services.security;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.context.SpringBootTest;
import ru.nsu.ccfit.mankiapp.dto.security.UserCredentials;
import ru.nsu.ccfit.mankiapp.exceptions.UserAlreadyExistsException;
import ru.nsu.ccfit.mankiapp.exceptions.WrongCredentialsException;
import ru.nsu.ccfit.mankiapp.exceptions.WrongFieldLengthException;
import ru.nsu.ccfit.mankiapp.exceptions.security.InvalidTokenException;
import ru.nsu.ccfit.mankiapp.model.data.UserData;
import ru.nsu.ccfit.mankiapp.model.repo.security.UserRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@SpringBootTest
class UserSecurityServiceTest {

    @Mock
    private UserRepository userRepository;

    private UserSecurityService userSecurityService;

    private final String CORRECT_NAME = "testName";
    private final String CORRECT_PASSWORD = "testPass";
    private final String LONG_VALUE = "1234567890".repeat(30);
    private final String CORRECT_TOKEN = "General Kenobi";

    @BeforeEach
    void setUp() {
        UserData user = new UserData(CORRECT_NAME, CORRECT_PASSWORD, CORRECT_TOKEN);
        Mockito.when(userRepository.login(CORRECT_NAME, CORRECT_PASSWORD)).thenReturn(Optional.of(user));
        Mockito.when(userRepository.findByUserToken(CORRECT_TOKEN)).thenReturn(Optional.of(user));
        Mockito.when(userRepository.findByUserName(CORRECT_NAME)).thenReturn(Optional.of(user));
        userSecurityService = new UserSecurityService(userRepository);
    }

    @Test
    void testCorrectLogin() {
        var loggedInUser = userSecurityService.login(new UserCredentials(CORRECT_NAME, "testPass"));
        assertNotNull(loggedInUser);
        assertDoesNotThrow(() -> new Throwable());
    }

    @Test
    void testLoginWithLongName() {
        assertThrows(WrongFieldLengthException.class,
                () -> userSecurityService.login(new UserCredentials(LONG_VALUE, CORRECT_PASSWORD)),
                "Username can not be longer than 255 characters");
    }

    @Test
    void testLoginWithLongPassword() {
        assertThrows(WrongFieldLengthException.class,
                () -> userSecurityService.login(new UserCredentials(CORRECT_NAME, LONG_VALUE)),
                "Password can not be longer than 255 characters");
    }

    @Test
    void testLoginWithWrongUserName() {
        assertThrows(WrongCredentialsException.class,
                () -> userSecurityService.login(new UserCredentials("wrongName", CORRECT_PASSWORD)),
                "Username or password are incorrect");
    }

    @Test
    void testLoginWithWrongPassword() {
        assertThrows(WrongCredentialsException.class,
                () -> userSecurityService.login(new UserCredentials(CORRECT_NAME, "wrongPass")),
                "Username or password are incorrect");
    }

    @Test
    void testRegisterWithLongUserName() {
        assertThrows(WrongFieldLengthException.class,
                () -> userSecurityService.register(new UserCredentials(LONG_VALUE, CORRECT_PASSWORD)),
                "Username can not be longer than 255 characters");
    }

    @Test
    void testRegisterWithLongPassword() {
        assertThrows(WrongFieldLengthException.class,
                () -> userSecurityService.register(new UserCredentials(CORRECT_NAME, LONG_VALUE)),
                "Password can not be longer than 255 characters");
    }

    @Test
    void testRegisterWithExistingUser() {
        assertThrows(UserAlreadyExistsException.class,
                () -> userSecurityService.register(new UserCredentials(CORRECT_NAME, CORRECT_PASSWORD)),
                "User with username " + CORRECT_NAME + " already exists");
    }

    @Test
    void testCorrectRegister() {
        var token = userSecurityService.register(new UserCredentials("new_name", "new_pass"));
        assertNotNull(token);
        assertDoesNotThrow(() -> new Throwable());
    }

    @Test
    void testLongToken() {
        assertThrows(WrongFieldLengthException.class,
                () -> userSecurityService.findByToken(LONG_VALUE),
                "Token can not be longer than 255 characters");
    }

    @Test
    void testInvalidToken() {
        var wrongToken = "Hello there";
        assertThrows(InvalidTokenException.class,
                () -> userSecurityService.findByToken(wrongToken),
                "Token " + wrongToken + " is invalid");
    }

    @Test
    void testCorrectToken() {
        var user = userSecurityService.findByToken(CORRECT_TOKEN).get();
        assertTrue(CORRECT_NAME.equals(user.getUsername())
                && CORRECT_PASSWORD.equals(user.getPassword()));
    }
}