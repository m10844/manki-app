package ru.nsu.ccfit.mankiapp.dto.mapper;

import org.junit.jupiter.api.Test;
import ru.nsu.ccfit.mankiapp.api.data.request.CardAddRequest;
import ru.nsu.ccfit.mankiapp.dto.CardAddingDto;

import static org.junit.jupiter.api.Assertions.*;

class DeckMapperTest {

    private final Integer deckId = 17;
    private final String cardQuestion = "Who?";
    private final String cardAnswer = "Me, Mario!";
    private final String cardQuestionImagePath = "bebra/bipki/card.png";
    private final String cardAnswerImagePath = "bipki/bebra/card.png";

    @Test
    void dtoFromCardAddRequest() {
        var addReq = new CardAddRequest(
                cardQuestion,
                cardQuestionImagePath,
                cardAnswer,
                cardAnswerImagePath,
                deckId
        );
        var rightDto = new CardAddingDto(
                cardQuestion,
                cardQuestionImagePath,
                cardAnswer,
                cardAnswerImagePath,
                0,
                deckId
        );
        assertEquals(rightDto, DeckMapper.dtoFromCardAddRequest(addReq));
    }
}