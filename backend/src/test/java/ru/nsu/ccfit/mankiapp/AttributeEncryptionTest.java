package ru.nsu.ccfit.mankiapp;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.nsu.ccfit.mankiapp.model.data.UserData;
import ru.nsu.ccfit.mankiapp.model.repo.security.UserRepository;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AttributeEncryptionTest {

    private static final String NAME = UUID.randomUUID().toString();
    private static final String PASSWORD = UUID.randomUUID().toString();
    private static final String TOKEN = UUID.randomUUID().toString();

    private int id;

    @Autowired
    private UserRepository userRepository;

    @Before
    public void setUp() {
        UserData user = new UserData(NAME, PASSWORD, TOKEN);
        id = userRepository.save(user).getId();
    }

    @Test
    public void testIsEncryptionCorrect() {
        var user = userRepository.findById(id).orElseThrow();
        Assertions.assertEquals(NAME, user.getUserName());
        Assertions.assertEquals(PASSWORD, user.getUserPassword());
        Assertions.assertEquals(TOKEN, user.getUserToken());
    }

    @After
    public void clearTestData() {
        var user = userRepository.findByUserName(NAME).get();
        userRepository.delete(user);
    }
}
