CREATE TABLE shared_decks (
    deck_id INTEGER PRIMARY KEY,
    share_token VARCHAR(50) NOT NULL
);