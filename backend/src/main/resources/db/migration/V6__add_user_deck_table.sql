CREATE TABLE user_deck
(
    user_id int references users(user_id),
    deck_id int references deck(deck_id),
    primary key (user_id, deck_id)
)