CREATE TABLE user_public_deck_score(
    user_id int8 NOT NULL,
    deck_id int8 NOT NULL,
    score int8 NOT NULL,
    primary key(user_id, deck_id)
)