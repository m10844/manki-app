CREATE TABLE card_tag
(
    card_id int references card(card_id),
    tag_id int references tag(tag_id),
    primary key(card_id, tag_id)
)