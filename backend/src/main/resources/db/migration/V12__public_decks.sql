ALTER TABLE deck
    ADD COLUMN is_public BOOLEAN DEFAULT FALSE ;

CREATE TABLE public_deck_stats
(
    deck_id    INTEGER PRIMARY KEY,
    fail_count INTEGER NOT NULL DEFAULT 0,
    hard_count INTEGER NOT NULL DEFAULT 0,
    good_count INTEGER NOT NULL DEFAULT 0,
    easy_count INTEGER NOT NULL DEFAULT 0
);