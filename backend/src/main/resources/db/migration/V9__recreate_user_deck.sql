DROP TABLE IF EXISTS user_deck;
CREATE TABLE user_deck
(
    id int primary key,
    user_id int references users(user_id),
    deck_id int references deck(deck_id)
);