package ru.nsu.ccfit.mankiapp.api.data.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.nsu.ccfit.mankiapp.dto.CardDto;

public record CardEditRequest(@JsonProperty("deck_id") Integer deckId, CardDto card) {
}
