package ru.nsu.ccfit.mankiapp.dto;

public record SharedDeckDto(
        Integer deckId,
        String shareToken
) {}
