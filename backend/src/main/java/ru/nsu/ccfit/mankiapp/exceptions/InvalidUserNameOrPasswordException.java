package ru.nsu.ccfit.mankiapp.exceptions;

public class InvalidUserNameOrPasswordException extends BaseException {
    public InvalidUserNameOrPasswordException(String message) {
        super(message, ErrorType.USER_NOT_FOUND_ERROR);
    }
}
