package ru.nsu.ccfit.mankiapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NonNull;

public record DeckEditDto(@NonNull Integer id, @NonNull @JsonProperty("deck_title") String deckTitle,
                          @NonNull @JsonProperty("deck_description") String deckDescription) {
}
