package ru.nsu.ccfit.mankiapp.api.data.response;

import ru.nsu.ccfit.mankiapp.dto.DeckDto;

public record DeckResponse(DeckDto deck) implements Response {
}
