package ru.nsu.ccfit.mankiapp.exceptions.security;

import ru.nsu.ccfit.mankiapp.exceptions.BaseException;
import ru.nsu.ccfit.mankiapp.exceptions.ErrorType;

public class InvalidTokenException extends BaseException {
    public InvalidTokenException(String message) {
        super(message, ErrorType.INVALID_TOKEN_ERROR);
    }
}
