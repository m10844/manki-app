package ru.nsu.ccfit.mankiapp.exceptions;

public class BadRequestException extends BaseException{
    public BadRequestException(String message) {
        super(message, ErrorType.BAD_REQUEST_ERROR);
    }
}
