package ru.nsu.ccfit.mankiapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

public record CardsIdsWithDeckIdDto(@JsonProperty(value = "cards_ids") List<Integer> cardsIds,
                                    @JsonProperty(value = "deck_id") Integer deckId) {
}
