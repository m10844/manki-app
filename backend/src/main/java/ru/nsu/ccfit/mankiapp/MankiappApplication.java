package ru.nsu.ccfit.mankiapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MankiappApplication {
	public static void main(String[] args) {
		SpringApplication.run(MankiappApplication.class, args);
	}
}
