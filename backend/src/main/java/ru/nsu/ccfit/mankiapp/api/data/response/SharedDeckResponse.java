package ru.nsu.ccfit.mankiapp.api.data.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public record SharedDeckResponse(
        @JsonProperty(value = "deck_id")
        Integer deckId,
        @JsonProperty(value = "share_token")
        String shareToken
) implements Response {
}
