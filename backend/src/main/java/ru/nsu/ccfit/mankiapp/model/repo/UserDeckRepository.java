package ru.nsu.ccfit.mankiapp.model.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.mankiapp.model.data.UserDeckData;

@Repository
public interface UserDeckRepository extends JpaRepository<UserDeckData, Integer> {
}
