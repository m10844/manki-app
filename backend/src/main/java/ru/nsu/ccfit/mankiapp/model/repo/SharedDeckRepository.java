package ru.nsu.ccfit.mankiapp.model.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.mankiapp.model.data.SharedDeckData;

import java.util.Optional;

@Repository
public interface SharedDeckRepository extends JpaRepository<SharedDeckData, Integer> {
    Optional<SharedDeckData> findSharedDeckDataByShareToken(String shareToken);
}
