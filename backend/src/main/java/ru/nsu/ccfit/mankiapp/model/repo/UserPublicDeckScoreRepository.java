package ru.nsu.ccfit.mankiapp.model.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.ccfit.mankiapp.model.data.UserPublicDeckScoreData;

import java.util.Optional;

public interface UserPublicDeckScoreRepository extends JpaRepository<UserPublicDeckScoreData, Integer> {

    Optional<UserPublicDeckScoreData> findByUserIdAndDeckId(Long userId, Long deckId);
}
