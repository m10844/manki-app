package ru.nsu.ccfit.mankiapp.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.ccfit.mankiapp.api.data.request.PointsRequest;
import ru.nsu.ccfit.mankiapp.api.data.request.TrainTimeRequest;
import ru.nsu.ccfit.mankiapp.api.data.response.ErrorResponse;
import ru.nsu.ccfit.mankiapp.api.data.response.PointsResponse;
import ru.nsu.ccfit.mankiapp.api.data.response.Response;
import ru.nsu.ccfit.mankiapp.dto.mapper.TrainPointsMapper;
import ru.nsu.ccfit.mankiapp.services.TrainService;

@RestController
@Tag(name = "Train", description = "Accrual of points to cards")
@RequestMapping("api/train")
public class TrainController {

    private final TrainService trainService;

    public TrainController(TrainService trainService) {
        this.trainService = trainService;
    }

    @Operation(summary = "Increase or decrease knowledge points on card depending on answer")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Card info with points",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = PointsResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "400",
                    description = "Card not found in provided deck",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "404",
                    description = "Provided deck not found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class)
                            )
                    }
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Wrong training type",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class)
                            )
                    }
            )
    })
    @PostMapping("/anki")
    public Response byAnki(@RequestBody PointsRequest request) {
        var points = trainService.addPointsWithAnki(TrainPointsMapper.requestToAnkiDto(request));

        return new PointsResponse(
                request.deckId(),
                request.cardId(),
                points
        );
    }

    @Operation(summary = "Increase or decrease knowledge points on card using integral system. Avaible only for public decks.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Card info with points",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = PointsResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "400",
                    description = "Card not found in provided deck",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "404",
                    description = "Provided deck not found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class)
                            )
                    }
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Wrong training type",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class)
                            )
                    }
            )
    })
    @PostMapping("/integral")
    public Response byIntegral(@RequestBody PointsRequest request) {
        var points = trainService.addPointByIntegral(TrainPointsMapper.requestToIntegralDto(request));

        return new PointsResponse(
                request.deckId(),
                request.cardId(),
                points - points.intValue() < 0.5 ? points.intValue() : points.intValue() + 1
        );
    }

    @Operation(summary = "Increase or decrease knowledge points on card depending on time")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Card info with points",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = PointsResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "400",
                    description = "Card not found in provided deck",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "404",
                    description = "Provided deck not found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class)
                            )
                    }
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Wrong training type",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class)
                            )
                    }
            )
    })
    @PostMapping("/time")
    public Response byTime(@RequestBody TrainTimeRequest request) {
        var points = trainService.addPointsByTime(TrainPointsMapper.requestTimeToDto(request));
        return new PointsResponse(
                request.deckId(),
                request.cardId(),
                points
        );
    }
}
