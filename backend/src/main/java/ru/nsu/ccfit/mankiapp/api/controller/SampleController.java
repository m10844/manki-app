package ru.nsu.ccfit.mankiapp.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/some-mapping")
public class SampleController {
  @GetMapping()
  public String get() {
    return "Some string returned from Spring backend!\n";
  }
}
