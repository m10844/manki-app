package ru.nsu.ccfit.mankiapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NonNull;

import java.util.List;

public record DeckDto(@NonNull Integer id, @NonNull @JsonProperty("deck_title") String deckTitle,
                      @NonNull @JsonProperty("deck_description") String deckDescription,
                      @NonNull @JsonProperty("is_public") Boolean isPublic, @NonNull List<CardDto> cards) {
}
