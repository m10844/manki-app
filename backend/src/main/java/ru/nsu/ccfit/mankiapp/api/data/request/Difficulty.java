package ru.nsu.ccfit.mankiapp.api.data.request;

public enum Difficulty {
    FAIL,
    HARD,
    GOOD,
    EASY
}
