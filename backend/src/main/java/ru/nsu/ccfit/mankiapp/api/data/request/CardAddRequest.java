package ru.nsu.ccfit.mankiapp.api.data.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NonNull;

public record CardAddRequest(String question, @JsonProperty("question_image_path") String questionImagePath,
                             String answer, @JsonProperty("answer_image_path") String answerImagePath,
                             @NonNull Integer deckId) {

}
