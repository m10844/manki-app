package ru.nsu.ccfit.mankiapp.api.data.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.nsu.ccfit.mankiapp.dto.DeckInfoDto;

public record DeckInfoResponse(@JsonProperty("deck_info") DeckInfoDto deckInfo) implements Response {
}
