package ru.nsu.ccfit.mankiapp.services;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.mankiapp.dto.*;

import ru.nsu.ccfit.mankiapp.dto.mapper.DeckMapper;
import ru.nsu.ccfit.mankiapp.exceptions.*;
import ru.nsu.ccfit.mankiapp.model.data.*;
import ru.nsu.ccfit.mankiapp.model.repo.*;
import ru.nsu.ccfit.mankiapp.model.repo.security.UserRepository;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.*;

@Service
public class DeckService {
    private final DeckRepository deckRepository;
    private final CardRepository cardRepository;
    private final UserRepository userRepository;
    private final UserDeckRepository userDeckRepository;
    private final SharedDeckRepository sharedDeckRepository;
    private final PublicDecksStatsRepository publicDecksStatsRepository;
    private static final String DECK_DOES_NOT_EXIST_STRING = "Deck with given identifier does not exist";

    @Autowired
    public DeckService(DeckRepository deckRepository, CardRepository cardRepository, UserRepository userRepository, UserDeckRepository userDeckRepository, SharedDeckRepository sharedDeckRepository, PublicDecksStatsRepository publicDecksStatsRepository) {
        this.deckRepository = deckRepository;
        this.cardRepository = cardRepository;
        this.userRepository = userRepository;
        this.userDeckRepository = userDeckRepository;
        this.sharedDeckRepository = sharedDeckRepository;
        this.publicDecksStatsRepository = publicDecksStatsRepository;
    }

    @Transactional
    public DeckDto getDeck(Integer id) {
        var deckOpt = deckRepository.findById(id);
        if (deckOpt.isPresent()) {
            var cardsOpt = cardRepository.findAllByDeckId(deckOpt.get().getId());
            return DeckMapper.deckFromData(deckOpt.get(), cardsOpt);
        } else {
            throw new WrongIdentifierException(DECK_DOES_NOT_EXIST_STRING);
        }
    }

    @Transactional
    public DeckDto modifyCards(CardWithDeckIdDto cards) {
        var deckData = deckRepository.findById(cards.deckId());
        if (deckData.isEmpty()) {
            throw new WrongIdentifierException("Deck with given identifier does not exist");
        }
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        var userOpt = userRepository.findByUserName(userName);
        var user = userOpt.orElseThrow();

        var deckUserExists = deckData.get()
                .getUsers().stream()
                .filter(e -> e.getUser().equals(user) && e.getDeck().equals(deckData.get()))
                .toList().isEmpty();
        if (deckUserExists) {
            throw new WrongIdentifierException("This deck not belong to you!");
        }

        var card = cards.card();
        var opt = cardRepository.findById(card.id());
        if (opt.isEmpty()) {
            throw new WrongIdentifierException("Card with id " + card.id() + " does not exist");
        }
        var cardData = opt.get();
        checkRequestLength(cardData);
        if (!Objects.equals(cardData.getDeckData().getId(), deckData.get().getId())) {
            throw new WrongDeckCardRelationException("Card with id "
                    + card.id()
                    + " does not belong to deck "
                    + deckData.get().getId());
        }
        var editInfo = cards.card();
        cardData.setDeckData(deckData.get());
        cardData.setCardQuestion(editInfo.question());
        cardData.setCardQuestionImagePath(editInfo.questionImagePath());
        cardData.setCardAnswer(editInfo.answer());
        cardData.setCardAnswerImagePath(editInfo.answerImagePath());
        cardData.setCardKnowledgePoints(editInfo.knowledgePoints());
        cardRepository.save(cardData);
        return getDeck(cards.deckId());
    }

    @Transactional
    public CardDto addCard(CardAddingDto card) {
        var deckData = deckRepository.findById(card.deckId());
        if (deckData.isEmpty()) {
            throw new WrongIdentifierException("Deck with given identifier does not exist");
        }
        var userOpt = userDataOpt();
        if (userOpt.isEmpty()) {
            throw new BadRequestException("User not found");
        }
        var deckUserExists = deckData.get()
                .getUsers().stream()
                .filter(e -> e.getUser().equals(userOpt.get()) && e.getDeck().equals(deckData.get()))
                .toList().isEmpty();
        if (deckUserExists) {
            throw new WrongIdentifierException("This deck not belong to you!");
        }
        var cardToAdd = DeckMapper.dataToAddFromCard(card);
        checkRequestLength(cardToAdd);
        cardToAdd.setDeckData(deckData.get());
        cardToAdd.setCardKnowledgePoints(0);
        return DeckMapper.cardFromData(cardRepository.save(cardToAdd));
    }

    @Transactional
    public DeckDto deleteCards(@NonNull CardsIdsWithDeckIdDto cards) {
        var deckData = deckRepository.findById(cards.deckId());
        if (deckData.isEmpty()) {
            throw new WrongIdentifierException(DECK_DOES_NOT_EXIST_STRING);
        }
        var cardsDataList = cardRepository.findAllById(cards.cardsIds());
        cardsDataList.forEach(card -> {
            if (!Objects.equals(card.getDeckData().getId(), deckData.get().getId())) {
                throw new WrongIdentifierException("Card with id "
                        + card.getId()
                        + " does not belong to deck "
                        + deckData.get().getId());
            }
        });
        cardRepository.deleteAll(cardsDataList);
        var cardsOpt = cardRepository.findAllByDeckId(deckData.get().getId());
        return DeckMapper.deckFromData(deckData.get(), cardsOpt);
    }


    @Transactional
    public DeckDto addDeck(DeckDto deck) {
        var userOpt = userDataOpt();
        if (userOpt.isEmpty()) {
            throw new InvalidUserNameOrPasswordException("User not found");
        }
        if (deck.deckTitle().length() > 255) {
            throw new WrongFieldLengthException("Deck title can not be longer than 255 characters");
        }
        if (deck.deckDescription().length() > 255) {
            throw new WrongFieldLengthException("Deck description can not be longer than 255 characters");
        }
        var savedDeck = deckRepository.save(DeckMapper.dataFromDeck(deck, deck.isPublic()));
        userDeckRepository.save(new UserDeckData(0, userOpt.get(), savedDeck));
        return DeckMapper.deckFromData(savedDeck);
    }

    @Transactional
    public void deleteDeck(Integer deckId) {
        var deckData = deckRepository.findById(deckId);
        if (deckData.isEmpty()) {
            throw new WrongIdentifierException(DECK_DOES_NOT_EXIST_STRING);
        }
        deckRepository.delete(deckData.get());
    }

    @Transactional
    public List<DeckInfoDto> getAllDecks(String name) {
        var userOpt = userRepository.findByUserName(name);
        if (userOpt.isEmpty()) {
            throw new BadRequestException("User not found");
        }
        try {
            return userRepository
                    .findById(userOpt.get().getId())
                    .orElseThrow()
                    .getDecks()
                    .stream()
                    .map(DeckMapper::userDeckDataToDecks)
                    .toList();
        } catch (Throwable e) {
            return Collections.emptyList();
        }
    }

    @Transactional
    public DeckInfoDto editDeck(DeckEditDto editDto) {
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        var userOpt = userRepository.findByUserName(userName);
        var user = userOpt.orElseThrow();

        var deckOpt = deckRepository.findById(editDto.id());
        if (deckOpt.isEmpty()) {
            throw new WrongIdentifierException("Deck with id " + editDto.id() + " does not exist");
        }
        var deck = deckOpt.get();

        var deckUserExists = deck
                .getUsers().stream()
                .filter(e -> e.getUser().equals(user) && e.getDeck().equals(deck))
                .toList().isEmpty();
        if (deckUserExists) {
            throw new WrongIdentifierException("This deck not belong to you!");
        }

        if (editDto.deckTitle().length() > 255) {
            throw new WrongFieldLengthException("Too long string for deck title");
        }
        if (editDto.deckDescription().length() > 255) {
            throw new WrongFieldLengthException("Too long string for deck description");
        }

        deck.setDeckTitle(editDto.deckTitle());
        deck.setDeckDescription(editDto.deckDescription());
        deckRepository.save(deck);
        return DeckMapper.deckInfoFromData(deck);
    }

    @Transactional
    public SharedDeckDto shareDeck(Integer deckId) {
        return DeckMapper.sharedDeckDataToDto(sharedDeckRepository.save(new SharedDeckData(deckId, createShareToken())));
    }

    @Transactional
    public DeckDto getSharedDeck(String shareToken) {
        var deck = deckRepository.findById(sharedDeckRepository
                        .findSharedDeckDataByShareToken(shareToken)
                        .orElseThrow(() -> new NotFoundException("Wrong share token"))
                        .getDeckId()
                )
                .orElseThrow(() -> new NotFoundException("Deck not found"));
        var user = userDataOpt().orElseThrow(() -> new WrongCredentialsException("User not found"));
        var check = user
                .getDecks()
                .stream().filter(d -> d.getDeck().getDeckTitle().equals(deck.getDeckTitle())
                        && d.getDeck().getCards().size() == deck.getCards().size()
                        && d.getDeck().getDeckDescription().equals(deck.getDeckDescription())
                )
                .count();
        if (check != 0) {
            throw new BadRequestException("You already have this deck");
        }
        userDeckRepository.save(new UserDeckData(0, user, deck));

        return DeckMapper.deckFromData(deck);
    }

    @Transactional
    public Boolean makeDeckPublic(Integer deckId) {
        var user = userDataOpt().orElseThrow(() -> new WrongCredentialsException("User not found"));
        var deck = userRepository.findByUserName(user.getUserName())
                .orElseThrow(() -> new NotFoundException("User doesn't have decks"))
                .getDecks().stream()
                .map(UserDeckData::getDeck)
                .filter(d -> Objects.equals(d.getId(), deckId))
                .findFirst()
                .orElseThrow(() -> new NotFoundException("Deck with id " + deckId + " not found"));
        if (deck.getIsPublic()) {
            return false;
        }
        deck.setIsPublic(true);
        publicDecksStatsRepository.save(new PublicDeckStats(deckId, 0, 0, 0, 0));
        return true;
    }

    @Transactional
    public List<DeckInfoDto> getAllPublic() {
        var decks = deckRepository.findDeckDataByIsPublic(true).orElse(Collections.emptyList());

        return decks.stream()
                .map(DeckMapper::deckInfoFromData)
                .toList();
    }

    private Optional<UserData> userDataOpt() {
        var name = SecurityContextHolder.getContext().getAuthentication().getName();
        return userRepository.findByUserName(name);
    }

    private String createShareToken() {
        return UUID.randomUUID().toString();
    }

    private void checkRequestLength(CardData cardData) {
        if (cardData.getCardQuestion().length() > 255) {
            throw new WrongFieldLengthException("Card question can not be longer than 255 characters");
        }
        if (cardData.getCardQuestionImagePath().length() > 255) {
            throw new WrongFieldLengthException("Card question image path can not be longer than 255 characters");
        }
        if (cardData.getCardAnswer().length() > 255) {
            throw new WrongFieldLengthException("Card answer can not be longer than 255 characters");
        }
        if (cardData.getCardAnswerImagePath().length() > 255) {
            throw new WrongFieldLengthException("Card answer image path can not be longer than 255 characters");
        }
    }
}
