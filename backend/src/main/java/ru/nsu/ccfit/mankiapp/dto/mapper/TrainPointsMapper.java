package ru.nsu.ccfit.mankiapp.dto.mapper;

import ru.nsu.ccfit.mankiapp.api.data.request.PointsRequest;
import ru.nsu.ccfit.mankiapp.api.data.request.TrainTimeRequest;
import ru.nsu.ccfit.mankiapp.dto.PointsAnkiDto;
import ru.nsu.ccfit.mankiapp.dto.PointsIntegralDto;
import ru.nsu.ccfit.mankiapp.dto.TrainTimeDto;

public class TrainPointsMapper {

    public TrainPointsMapper() {
        throw new IllegalStateException("Mapper class cannot be instantiated");
    }

    public static PointsAnkiDto requestToAnkiDto(PointsRequest request) {
        return new PointsAnkiDto(
                request.deckId(),
                request.cardId(),
                request.difficulty()
        );
    }

    public static PointsIntegralDto requestToIntegralDto(PointsRequest request) {
        return new PointsIntegralDto(
                request.deckId(),
                request.cardId(),
                request.difficulty()
        );
    }

    public static TrainTimeDto requestTimeToDto(TrainTimeRequest request) {
        return new TrainTimeDto(
                request.deckId(),
                request.cardId(),
                request.userInput(),
                request.timeTaken()
        );
    }
}
