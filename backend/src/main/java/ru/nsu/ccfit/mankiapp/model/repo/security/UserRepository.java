package ru.nsu.ccfit.mankiapp.model.repo.security;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.mankiapp.model.data.UserData;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserData, Integer> {

    @Query(value = "SELECT u FROM UserData u where u.userName = :username and u.userPassword = :password ")
    Optional<UserData> login(String username, String password);

    Optional<UserData> findByUserName(String userName);

    Optional<UserData> findByUserToken(String token);

    @Modifying
    @Query(value = "UPDATE UserData u SET u.userPassword = :newPassword WHERE u.id = :id")
    void updatePassword(Integer id, String newPassword);
}
