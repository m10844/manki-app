package ru.nsu.ccfit.mankiapp.model.data;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class UserPublicDeckScoreId implements Serializable {
    private Long userId;
    private Long deckId;

}
