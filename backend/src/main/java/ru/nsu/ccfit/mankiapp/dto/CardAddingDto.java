package ru.nsu.ccfit.mankiapp.dto;

import lombok.NonNull;

public record CardAddingDto(String question, String questionImagePath, String answer, String answerImagePath,
                            @NonNull Integer knowledgePoints, @NonNull Integer deckId) {

}
