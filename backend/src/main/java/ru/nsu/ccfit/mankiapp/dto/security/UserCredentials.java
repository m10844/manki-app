package ru.nsu.ccfit.mankiapp.dto.security;

import lombok.NonNull;

public record UserCredentials(@NonNull String username, @NonNull String password) {

}
