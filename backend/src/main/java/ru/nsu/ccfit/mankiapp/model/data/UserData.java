package ru.nsu.ccfit.mankiapp.model.data;


import lombok.*;

import org.hibernate.Hibernate;

import ru.nsu.ccfit.mankiapp.dto.security.Encryptor;

import javax.persistence.*;

import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "users")
public class UserData {

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "user_name", unique = true, nullable = false)
    @NonNull
    private String userName;

    @Column(name = "user_password", nullable = false)
    @Convert(converter = Encryptor.class)
    @NonNull
    private String userPassword;


    @Column(name = "user_token", unique = true, nullable = false)
    @Convert(converter = Encryptor.class)
    @NonNull
    private String userToken;

    @OneToMany(mappedBy = "user")
    Set<UserDeckData> decks;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        UserData userData = (UserData) o;
        return Objects.equals(id, userData.id);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
