package ru.nsu.ccfit.mankiapp.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.mankiapp.api.data.request.ChangePasswordRequest;
import ru.nsu.ccfit.mankiapp.api.data.response.ChangePasswordResponse;
import ru.nsu.ccfit.mankiapp.api.data.response.ErrorResponse;
import ru.nsu.ccfit.mankiapp.api.data.response.ProfileResponse;
import ru.nsu.ccfit.mankiapp.api.data.response.Response;
import ru.nsu.ccfit.mankiapp.dto.mapper.UserMapper;
import ru.nsu.ccfit.mankiapp.services.ProfileService;

@AllArgsConstructor
@RestController
@Tag(name = "Profile", description = "All information about user")
@RequestMapping("api/profile")
public class UserProfileController {

    private final ProfileService profileService;

    @Operation(summary = "Get information about user")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Profile",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ProfileResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "500",
                    description = "Internal server error",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    })
    })
    @GetMapping("/info")
    public Response getProfileInfo() {
        return UserMapper.dtoToProfileResponse(profileService.getProfileInfo());
    }

    @Operation(summary = "Change password")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Password changed",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ChangePasswordResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "500",
                    description = "Internal server error",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    })
    })
    @PostMapping("/change_password")
    public Response changePassword(@RequestBody ChangePasswordRequest request) {
        profileService.updatePassword(request.oldPassword(), request.newPassword());

        return new ChangePasswordResponse("OK");
    }
}
