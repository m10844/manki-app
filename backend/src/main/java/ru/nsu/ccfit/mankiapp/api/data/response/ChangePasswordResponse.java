package ru.nsu.ccfit.mankiapp.api.data.response;


import ru.nsu.ccfit.mankiapp.api.data.response.Response;

public record ChangePasswordResponse(String message) implements Response {
}
