package ru.nsu.ccfit.mankiapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NonNull;
import lombok.Value;

public record CardWithDeckIdDto(@NonNull CardDto card, @NonNull @JsonProperty("deck_id") Integer deckId) {
}
