package ru.nsu.ccfit.mankiapp.exceptions;

public abstract class BaseException extends IllegalArgumentException {

    protected final ErrorType type;

    protected BaseException(String message, ErrorType type) {
        super(message);
        this.type = type;
    }

    public ErrorType getType() {
        return type;
    }
}
