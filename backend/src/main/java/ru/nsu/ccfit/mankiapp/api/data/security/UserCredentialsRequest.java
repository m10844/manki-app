package ru.nsu.ccfit.mankiapp.api.data.security;


public record UserCredentialsRequest(String username, String password) {
}
