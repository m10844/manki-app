package ru.nsu.ccfit.mankiapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.mankiapp.api.data.request.Difficulty;
import ru.nsu.ccfit.mankiapp.dto.PointsAnkiDto;
import ru.nsu.ccfit.mankiapp.dto.PointsIntegralDto;
import ru.nsu.ccfit.mankiapp.dto.TrainTimeDto;
import ru.nsu.ccfit.mankiapp.exceptions.InvalidTrainingTypeException;
import ru.nsu.ccfit.mankiapp.exceptions.NotFoundException;
import ru.nsu.ccfit.mankiapp.exceptions.WrongCredentialsException;
import ru.nsu.ccfit.mankiapp.exceptions.WrongDeckCardRelationException;
import ru.nsu.ccfit.mankiapp.model.data.PublicDeckStats;
import ru.nsu.ccfit.mankiapp.model.data.UserData;
import ru.nsu.ccfit.mankiapp.model.data.UserPublicDeckScoreData;
import ru.nsu.ccfit.mankiapp.model.repo.DeckRepository;
import ru.nsu.ccfit.mankiapp.model.repo.PublicDecksStatsRepository;
import ru.nsu.ccfit.mankiapp.model.repo.UserPublicDeckScoreRepository;
import ru.nsu.ccfit.mankiapp.model.repo.security.UserRepository;

import javax.transaction.Transactional;
import java.util.Objects;

@Service
public class TrainService {
    private static final String DECK_DOES_NOT_EXIST_STRING = "Deck with given identifier does not exist";

    private final DeckRepository deckRepository;

    private final UserRepository userRepository;

    private final PublicDecksStatsRepository publicDecksStatsRepository;

    private final UserPublicDeckScoreRepository userPublicDeckScoreRepository;

    @Autowired
    public TrainService(DeckRepository deckRepository, UserRepository userRepository, PublicDecksStatsRepository publicDecksStatsRepository, UserPublicDeckScoreRepository userPublicDeckScoreRepository) {
        this.deckRepository = deckRepository;
        this.userRepository = userRepository;
        this.publicDecksStatsRepository = publicDecksStatsRepository;
        this.userPublicDeckScoreRepository = userPublicDeckScoreRepository;
    }

    @Transactional
    public Integer addPointsWithAnki(PointsAnkiDto dto) {
        if (isPublic(dto.deckId())) {
            throw new InvalidTrainingTypeException("Only integral system available for public decks");
        }
        var card = deckRepository
                .findByUserId(
                        userData().getId()
                )
                .orElseThrow(() -> new NotFoundException("User doesn't have decks"))
                .stream()
                .filter(ud -> Objects.equals(ud.getDeck().getId(), dto.deckId()))
                .limit(1)
                .flatMap(ud -> ud.getDeck().getCards().stream())
                .filter(c -> Objects.equals(c.getId(), dto.cardId()))
                .findFirst()
                .orElseThrow(() -> new WrongDeckCardRelationException("Card with id " + dto.cardId() + " not found in deck with id " + dto.deckId()));
        var pointsAdded = dto.mapDifficultyToPoints();
        var newPoints = card.getCardKnowledgePoints() + pointsAdded;
        if (newPoints < 0) {
            newPoints = 0;
        }
        card.setCardKnowledgePoints(newPoints);

        return pointsAdded;
    }

    public Double addPointByIntegral(PointsIntegralDto dto) {
        var card = deckRepository
                .findByUserId(
                        userData().getId()
                )
                .orElseThrow(() -> new NotFoundException("User doesn't have decks"))
                .stream()
                .filter(ud -> Objects.equals(ud.getDeck().getId(), dto.deckId()))
                .limit(1)
                .flatMap(ud -> ud.getDeck().getCards().stream())
                .filter(c -> Objects.equals(c.getId(), dto.cardId()))
                .findFirst()
                .orElseThrow(() -> new WrongDeckCardRelationException("Card with id " + dto.cardId() + " not found in deck with id " + dto.deckId()));

        var deckOpt = deckRepository.findById(dto.deckId());
        var deck = deckOpt.orElseThrow();
        if (!deck.getIsPublic()) {
            throw new InvalidTrainingTypeException("Integral training type is only available for public decks");
        }
        var user = userData();
        var deckStatsOpt = publicDecksStatsRepository.findById(dto.deckId());
        PublicDeckStats deckStats;
        if (deckStatsOpt.isEmpty()) {
            deckStats = new PublicDeckStats();
        } else {
            deckStats = deckStatsOpt.get();
        }
        var userDeckScoreOpt = userPublicDeckScoreRepository
                .findByUserIdAndDeckId((long) user.getId(), (long) dto.deckId());
        UserPublicDeckScoreData userDeckScore;
        if (userDeckScoreOpt.isEmpty()) {
            userDeckScore = new UserPublicDeckScoreData((long) user.getId(), (long) dto.deckId(), 0.0);
        } else {
            userDeckScore = userDeckScoreOpt.get();
        }
        if (Double.isNaN(userDeckScore.getScore())) {
            userDeckScore.setScore(0.0);
        }
        double points = mapDifficultyToPointByStats(dto.difficulty(), deckStats);
        if (Double.isNaN(points)) {
            points = 0.0;
        }
        var newPoints = userDeckScore.getScore() + points;
        if (newPoints < 0) {
            newPoints = 0;
        }
        userDeckScore.setScore(newPoints);
        card.setCardKnowledgePoints(card.getCardKnowledgePoints() + (int) points);
        switch (dto.difficulty()) {
            case FAIL -> deckStats.setFailCount(deckStats.getFailCount() + 1);
            case GOOD -> deckStats.setGoodCount(deckStats.getGoodCount() + 1);
            case EASY -> deckStats.setEasyCount(deckStats.getEasyCount() + 1);
            default -> deckStats.setHardCount(deckStats.getHardCount() + 1);
        }
        userPublicDeckScoreRepository.save(userDeckScore);
        publicDecksStatsRepository.save(deckStats);
        return points;
    }

    public Integer addPointsByTime(TrainTimeDto dto) {
        if (isPublic(dto.deckId())) {
            throw new InvalidTrainingTypeException("Only integral system available for public decks");
        }
        var card = deckRepository
                .findByUserId(
                        userData().getId()
                )
                .orElseThrow(() -> new NotFoundException("User doesn't have decks"))
                .stream()
                .filter(ud -> Objects.equals(ud.getDeck().getId(), dto.deckId()))
                .limit(1)
                .flatMap(ud -> ud.getDeck().getCards().stream())
                .filter(c -> Objects.equals(c.getId(), dto.cardId()))
                .findFirst()
                .orElseThrow(() -> new WrongDeckCardRelationException("Card with id " + dto.cardId() + " not found in deck with id " + dto.deckId()));
        var rightAnswerLength = card.getCardAnswer().length();
        var typingSpeed = dto.timeTaken() / rightAnswerLength;
        var points = mapTimeToPoints(typingSpeed);
        var newPoints = card.getCardKnowledgePoints() + points;
        if (newPoints < 0) {
            newPoints = 0;
        }
        card.setCardKnowledgePoints(newPoints);

        return points;
    }

    public boolean isPublic(Integer deckId) {
        return publicDecksStatsRepository.findById(deckId).isPresent();
    }

    public Integer mapTimeToPoints(Double speed) {
        if (speed < 0.5) {
            return 2;
        } else if (speed < 1) {
            return 1;
        } else if (speed > 2) {
            return -1;
        } else {
            return 0;
        }
    }

    public double mapDifficultyToPointByStats(Difficulty difficulty, PublicDeckStats stats) {
        int totalAnswers = stats.getEasyCount() + stats.getGoodCount() + stats.getHardCount() + stats.getFailCount();
        double eps = 0.001;
        switch (difficulty) {
            case FAIL -> {
                double part = ((double) stats.getFailCount()) / totalAnswers + eps;
                double coefficient = 1 / (part);
                return (coefficient * -1) % 10.0;
            }
            case GOOD -> {
                double betterAnswersPart = ((double) stats.getEasyCount()) / totalAnswers + eps;
                double coefficient;
                if (betterAnswersPart > 0.5) {
                    coefficient = 1 - betterAnswersPart;
                } else {
                    coefficient = 1 / betterAnswersPart;
                }
                return coefficient % 10.0;
            }
            case EASY -> {
                double part = ((double) stats.getEasyCount()) / totalAnswers + eps;
                double coefficient = 1 / (part);
                return (coefficient * 2) % 10.0;
            }
            default -> {
                return 0.0;
            }
        }
    }

    private UserData userData() {
        var name = SecurityContextHolder.getContext().getAuthentication().getName();
        return userRepository.findByUserName(name)
                .orElseThrow(() -> new WrongCredentialsException("User not found"));
    }
}
