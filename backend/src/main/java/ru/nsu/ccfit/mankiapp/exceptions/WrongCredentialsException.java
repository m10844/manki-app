package ru.nsu.ccfit.mankiapp.exceptions;

public class WrongCredentialsException extends BaseException {
    public WrongCredentialsException(String message) {
        super(message, ErrorType.WRONG_CREDENTIALS_ERROR);
    }
}
