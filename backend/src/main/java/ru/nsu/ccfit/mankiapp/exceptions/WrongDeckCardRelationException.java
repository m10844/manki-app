package ru.nsu.ccfit.mankiapp.exceptions;

public class WrongDeckCardRelationException extends BaseException{
    public WrongDeckCardRelationException(String message) {
        super(message, ErrorType.WRONG_DECK_CARD_RELATION);
    }
}
