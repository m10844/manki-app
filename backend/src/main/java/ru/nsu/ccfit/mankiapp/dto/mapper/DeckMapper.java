package ru.nsu.ccfit.mankiapp.dto.mapper;

import lombok.NonNull;
import ru.nsu.ccfit.mankiapp.api.data.request.CardAddRequest;
import ru.nsu.ccfit.mankiapp.dto.*;
import ru.nsu.ccfit.mankiapp.model.data.*;

import java.util.*;

public class DeckMapper {
    public static DeckInfoDto deckInfoFromData(DeckData deckData) {
        return new DeckInfoDto(
                deckData.getId(),
                deckData.getDeckTitle(),
                deckData.getDeckDescription(),
                deckData.getIsPublic(),
                deckData.getCards().size()
        );
    }

    public static DeckDto deckFromData(DeckData deckData) {
        if (deckData.getCards() == null) {
            return deckFromData(deckData, Collections.emptyList());
        }
        return deckFromData(deckData, deckData.getCards().stream().toList());
    }

    public static DeckDto deckFromData(DeckData deckData, List<CardData> cardsData) {
        return new DeckDto(deckData.getId(),
                deckData.getDeckTitle(),
                deckData.getDeckDescription(),
                deckData.getIsPublic(),
                sort(cardsData.stream().map(DeckMapper::cardFromData).toList()));
    }

    /**
     * @param collection collection to be sorted
     * @return sorted collection by knowledge points
     */
    private static <T extends CardDto> List<T> sort(@NonNull Collection<T> collection) {
        return collection.stream()
                .sorted(Comparator.comparingInt(CardDto::knowledgePoints))
                .toList();
    }

    public static DeckData dataFromDeck(DeckDto deckDto, Boolean isPublic) {
        return new DeckData(deckDto.deckTitle(), deckDto.deckDescription(), isPublic);
    }

    public static CardDto cardFromData(CardData cardData) {
        return new CardDto(cardData.getId(),
                cardData.getCardQuestion(),
                cardData.getCardQuestionImagePath(),
                cardData.getCardAnswer(),
                cardData.getCardAnswerImagePath(),
                cardData.getCardKnowledgePoints());
    }

    public static CardData dataToAddFromCard(CardAddingDto cardAddingDto) {
        CardData card = new CardData();
        card.setCardQuestion(cardAddingDto.question());
        card.setCardQuestionImagePath(cardAddingDto.questionImagePath());
        card.setCardAnswer(cardAddingDto.answer());
        card.setCardAnswerImagePath(cardAddingDto.answerImagePath());
        card.setCardKnowledgePoints(cardAddingDto.knowledgePoints());

        return card;
    }

    public static CardAddingDto dtoFromCardAddRequest(CardAddRequest cardAddRequest) {
        return new CardAddingDto(
                cardAddRequest.question(),
                cardAddRequest.questionImagePath(),
                cardAddRequest.answer(),
                cardAddRequest.answerImagePath(),
                0,
                cardAddRequest.deckId()
        );
    }

    public static DeckInfoDto userDeckDataToDecks(UserDeckData userDeckData) {
        var deck = userDeckData.getDeck();
        return deckInfoFromData(deck);
    }

    public static SharedDeckDto sharedDeckDataToDto(SharedDeckData sharedDeckData) {
        return new SharedDeckDto(sharedDeckData.getDeckId(), sharedDeckData.getShareToken());
    }
}
