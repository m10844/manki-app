package ru.nsu.ccfit.mankiapp.model.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.mankiapp.model.data.CardData;

import java.util.List;

@Repository
public interface CardRepository extends JpaRepository<CardData, Integer> {
    @Query("select card from CardData card where card.deckData.id = :id")
    List<CardData> findAllByDeckId(Integer id);
}
