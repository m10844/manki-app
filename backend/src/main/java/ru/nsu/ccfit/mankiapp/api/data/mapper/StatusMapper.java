package ru.nsu.ccfit.mankiapp.api.data.mapper;

import org.springframework.http.HttpStatus;
import ru.nsu.ccfit.mankiapp.exceptions.BaseException;

public class StatusMapper {

    public static HttpStatus mapStatus(BaseException exception) {
        switch (exception.getType()) {
            case INVALID_TOKEN_ERROR -> {
                return HttpStatus.FORBIDDEN;
            }
            case NOT_FOUND -> {
                return HttpStatus.NOT_FOUND;
            }
        }

        return HttpStatus.BAD_REQUEST;
    }

    public static Integer mapStatusValue(BaseException exception) {
        switch (exception.getType()) {
            case NOT_FOUND -> {
                return 404;
            }
            case INVALID_TOKEN_ERROR, WRONG_CREDENTIALS_ERROR -> {
                return 401;
            }
        }

        return 500;
    }
}
