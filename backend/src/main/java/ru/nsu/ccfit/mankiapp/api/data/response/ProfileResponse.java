package ru.nsu.ccfit.mankiapp.api.data.response;

public record ProfileResponse(String name, Integer totalDecks, Double averageKnowledgePoints) implements Response {
}
