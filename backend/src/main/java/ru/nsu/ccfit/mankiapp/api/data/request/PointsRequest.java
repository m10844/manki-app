package ru.nsu.ccfit.mankiapp.api.data.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public record PointsRequest(
        @JsonProperty("deck_id")
        Integer deckId,
        @JsonProperty("card_id")
        Integer cardId,
        @JsonProperty("answer")
        Difficulty difficulty
) {
}