package ru.nsu.ccfit.mankiapp.dto;

public record ProfileDto(
        String name,
        Integer totalDecks,
        Double averageKnowledgePoints
) {
}
