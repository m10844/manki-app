package ru.nsu.ccfit.mankiapp.config;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import ru.nsu.ccfit.mankiapp.exceptions.security.InvalidTokenException;
import ru.nsu.ccfit.mankiapp.services.security.UserSecurityService;

import java.util.Optional;
@Component
public class AuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
    private final UserSecurityService userSecurityService;

    public AuthenticationProvider(UserSecurityService userSecurityService) {
        this.userSecurityService = userSecurityService;
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        //
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        var token = authentication.getCredentials();
        return Optional
                .ofNullable(token)
                .map(String::valueOf)
                .flatMap(userSecurityService::findByToken)
                .orElseThrow(() -> new InvalidTokenException("Username with token " + token + " not found"));
    }
}
