package ru.nsu.ccfit.mankiapp.model.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.mankiapp.model.data.DeckData;
import ru.nsu.ccfit.mankiapp.model.data.UserData;
import ru.nsu.ccfit.mankiapp.model.data.UserDeckData;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface DeckRepository extends JpaRepository<DeckData, Integer> {

    @Query("SELECT user.decks FROM UserData user")
    Optional<Set<UserDeckData>> findByUserId(Integer id);

    Optional<List<DeckData>> findDeckDataByIsPublic(Boolean isPublic);
}
