package ru.nsu.ccfit.mankiapp.model.data;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "shared_decks")
public class SharedDeckData {
    @Id
    @Column(name = "deck_id")
    private Integer deckId;

    @Column(name = "share_token")
    private String shareToken;
}
