package ru.nsu.ccfit.mankiapp.dto.mapper;

import ru.nsu.ccfit.mankiapp.api.data.response.ProfileResponse;
import ru.nsu.ccfit.mankiapp.dto.ProfileDto;
import ru.nsu.ccfit.mankiapp.model.data.UserData;

public class UserMapper {
    public static ProfileDto userDataToProfileDto(UserData user) {
        var totalDecks = 0;
        double averagePoints = 0;
        if (user.getDecks() != null) {
            totalDecks = user.getDecks().size();
            averagePoints = user.getDecks().stream()
                    .flatMap(userDeck -> userDeck.getDeck().getCards().stream())
                    .mapToDouble(cardData -> cardData == null ? 0D : cardData.getCardKnowledgePoints().doubleValue())
                    .average()
                    .orElse(0.0);
        }
        return new ProfileDto(user.getUserName(),
                totalDecks,
                averagePoints
        );
    }

    public static ProfileResponse dtoToProfileResponse(ProfileDto profileDto) {
        return new ProfileResponse(
                profileDto.name(),
                profileDto.totalDecks(),
                profileDto.averageKnowledgePoints()
        );
    }
}
