package ru.nsu.ccfit.mankiapp.services.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.mankiapp.dto.security.UserCredentials;
import ru.nsu.ccfit.mankiapp.exceptions.BaseException;
import ru.nsu.ccfit.mankiapp.exceptions.UserAlreadyExistsException;
import ru.nsu.ccfit.mankiapp.exceptions.WrongCredentialsException;
import ru.nsu.ccfit.mankiapp.exceptions.WrongFieldLengthException;
import ru.nsu.ccfit.mankiapp.exceptions.security.InvalidTokenException;
import ru.nsu.ccfit.mankiapp.model.data.UserData;
import ru.nsu.ccfit.mankiapp.model.repo.security.UserRepository;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserSecurityService {
    private final UserRepository userRepository;

    @Autowired
    public UserSecurityService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String login(UserCredentials credentials) throws BaseException {
        //Calling this method once to acquire credentials
        if (credentials.username().length() > 255) {
            throw new WrongFieldLengthException("Username can not be longer than 255 characters");
        }
        if (credentials.password().length() > 255) {
            throw new WrongFieldLengthException("Password can not be longer than 255 characters");
        }
        var userOpt = userRepository.login(credentials.username(), credentials.password());
        if (userOpt.isEmpty()) {
            throw new WrongCredentialsException("Username or password are incorrect");
        }
        var user = userOpt.get();

        String token = UUID.randomUUID().toString();
        user.setUserToken(token);
        userRepository.save(user);
        return token;
    }

    public String register(UserCredentials credentials) {
        if (credentials.username().length() > 255) {
            throw new WrongFieldLengthException("Username can not be longer than 255 characters");
        }
        if (credentials.password().length() > 255) {
            throw new WrongFieldLengthException("Password can not be longer than 255 characters");
        }
        var isUserExist = userRepository.findByUserName(credentials.username()).isPresent();
        if (isUserExist) {
            throw new UserAlreadyExistsException("User with username " + credentials.username() + " already exists");
        }
        var newUser = new UserData();
        newUser.setUserName(credentials.username());
        newUser.setUserPassword(credentials.password());
        String token = UUID.randomUUID().toString();
        newUser.setUserToken(token);
        userRepository.save(newUser);
        return token;
    }

    public Optional<User> findByToken(String token) {
        //Method used in every secure function to check if token is valid
        if (token.length() > 255) {
            throw new WrongFieldLengthException("Token can not be longer than 255 characters");
        }
        var userOpt = userRepository.findByUserToken(token);
        if (userOpt.isEmpty()) {
            throw new InvalidTokenException("Token " + token + " is invalid");
        }
        var user = userOpt.get();
        return Optional.of(new User(
                user.getUserName(),
                user.getUserPassword(),
                true,
                true,
                true,
                true,
                AuthorityUtils.createAuthorityList("USER")
        ));
    }
}
