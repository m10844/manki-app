package ru.nsu.ccfit.mankiapp.dto;

import ru.nsu.ccfit.mankiapp.api.data.request.Difficulty;

public record PointsAnkiDto(
        Integer deckId,
        Integer cardId,
        Difficulty difficulty
) {

    public Integer mapDifficultyToPoints() {
        switch (difficulty) {
            case FAIL -> {
                return -1;
            }
            case GOOD -> {
                return 1;
            }
            case EASY -> {
                return 2;
            }
            default -> {
                return 0;
            }
        }
    }

}
