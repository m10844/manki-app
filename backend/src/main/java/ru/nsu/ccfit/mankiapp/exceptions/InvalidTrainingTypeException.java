package ru.nsu.ccfit.mankiapp.exceptions;

public class InvalidTrainingTypeException extends BaseException {
    public InvalidTrainingTypeException(String message) {
        super(message, ErrorType.WRONG_TRAINING_TYPE_ERROR);
    }
}
