package ru.nsu.ccfit.mankiapp.api.data.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public record TrainTimeRequest(
        @JsonProperty("deck_id")
        Integer deckId,
        @JsonProperty("card_id")
        Integer cardId,
        @JsonProperty("user_input")
        String userInput,
        @JsonProperty("time_taken")
        Double timeTaken
) {
}
