package ru.nsu.ccfit.mankiapp.api.data.response;

import ru.nsu.ccfit.mankiapp.dto.DeckInfoDto;

import java.util.List;

public record AllDecksResponse(List<DeckInfoDto> deckInfo) implements Response {
}
