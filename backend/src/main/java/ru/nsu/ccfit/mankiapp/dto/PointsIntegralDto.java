package ru.nsu.ccfit.mankiapp.dto;

import lombok.Value;
import ru.nsu.ccfit.mankiapp.api.data.request.Difficulty;

public record PointsIntegralDto(Integer deckId, Integer cardId, Difficulty difficulty) {
}
