package ru.nsu.ccfit.mankiapp.api.data.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public record DeckAddRequest(@JsonProperty("deck_title") String deckTitle,
                             @JsonProperty("deck_description") String deckDescription) {
}
