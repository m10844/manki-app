package ru.nsu.ccfit.mankiapp.model.data;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
//@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "card")
public class CardData {

    @Id
    @Column(name = "card_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "card_question")
    private String cardQuestion;

    @Column(name = "card_question_image_path")
    private String cardQuestionImagePath;

    @Column(name = "card_answer")
    private String cardAnswer;

    @Column(name = "card_answer_image_path")
    private String cardAnswerImagePath;

    @Column(name = "card_knowledge_points", nullable = false)
    @NonNull
    private Integer cardKnowledgePoints;

    @ManyToOne
    @JoinColumn(name = "deck_id")
//    @OnDelete(action = OnDeleteAction.CASCADE)
    @NonNull
    private DeckData deckData;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        CardData cardData = (CardData) o;
        return Objects.equals(id, cardData.id);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
