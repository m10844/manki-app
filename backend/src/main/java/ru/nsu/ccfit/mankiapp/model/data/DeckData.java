package ru.nsu.ccfit.mankiapp.model.data;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Table(name = "deck")
public class DeckData {
    @Id
    @Column(name = "deck_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "deck_title", nullable = false)
    @NonNull
    private String deckTitle;

    @Column(name = "deck_description", nullable = false)
    @NonNull
    private String deckDescription;

    @Column(name = "is_public")
    @NonNull
    private Boolean isPublic;

    @OneToMany(mappedBy = "deckData", cascade = CascadeType.ALL)
    private Set<CardData> cards;

    @OneToMany(mappedBy = "deck", cascade = CascadeType.ALL)
    Set<UserDeckData> users;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        DeckData deckData = (DeckData) o;
        return Objects.equals(id, deckData.id);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
