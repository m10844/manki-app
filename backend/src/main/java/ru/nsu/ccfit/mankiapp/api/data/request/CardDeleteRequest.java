package ru.nsu.ccfit.mankiapp.api.data.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record CardDeleteRequest(@JsonProperty("deck_id") Integer deckId,
                                @JsonProperty("cards_ids") List<Integer> cardsIds) {
}
