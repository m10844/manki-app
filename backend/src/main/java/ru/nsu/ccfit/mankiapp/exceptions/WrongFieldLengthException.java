package ru.nsu.ccfit.mankiapp.exceptions;

public class WrongFieldLengthException extends BaseException {
    public WrongFieldLengthException(String message) {
        super(message, ErrorType.WRONG_FIELD_LENGTH_ERROR);
    }
}
