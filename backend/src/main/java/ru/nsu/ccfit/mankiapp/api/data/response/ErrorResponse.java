package ru.nsu.ccfit.mankiapp.api.data.response;


public record ErrorResponse(Integer status, String message) implements Response {

}
