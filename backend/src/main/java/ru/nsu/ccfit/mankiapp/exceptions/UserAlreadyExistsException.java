package ru.nsu.ccfit.mankiapp.exceptions;

public class UserAlreadyExistsException extends BaseException{
    public UserAlreadyExistsException(String message) {
        super(message, ErrorType.USER_ALREADY_EXISTS_ERROR);
    }
}
