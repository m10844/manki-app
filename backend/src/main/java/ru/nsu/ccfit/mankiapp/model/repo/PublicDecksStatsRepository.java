package ru.nsu.ccfit.mankiapp.model.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.ccfit.mankiapp.model.data.PublicDeckStats;

public interface PublicDecksStatsRepository extends JpaRepository<PublicDeckStats, Integer> {
}
