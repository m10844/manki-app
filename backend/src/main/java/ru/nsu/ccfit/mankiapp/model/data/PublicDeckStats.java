package ru.nsu.ccfit.mankiapp.model.data;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "public_deck_stats")
public class PublicDeckStats {

    @Id
    @Column(name = "deck_id")
    private Integer deckId;

    @Column(name = "fail_count")
    @NonNull
    Integer failCount = 0;

    @Column(name = "hard_count")
    @NonNull
    Integer hardCount = 0;

    @Column(name = "good_count")
    @NonNull
    Integer goodCount = 0;

    @Column(name = "easy_count")
    @NonNull
    Integer easyCount = 0;
}