package ru.nsu.ccfit.mankiapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @param id @JsonProperty("card_id")
 */
public record CardDto(Integer id, String question, @JsonProperty("question_image_path") String questionImagePath,
                      String answer, @JsonProperty("answer_image_path") String answerImagePath,
                      @JsonProperty("knowledge_points") Integer knowledgePoints) {
}
