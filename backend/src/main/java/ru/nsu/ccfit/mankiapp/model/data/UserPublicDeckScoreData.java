package ru.nsu.ccfit.mankiapp.model.data;

import lombok.*;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "user_public_deck_score")
@IdClass(UserPublicDeckScoreId.class)
public class UserPublicDeckScoreData {
    @Id
    @Column(name = "user_id")
    private Long userId;

    @Id
    @Column(name = "deck_id")
    private Long deckId;

    @Column(name = "score")
    @NonNull
    private Double score;
}
