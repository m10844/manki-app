package ru.nsu.ccfit.mankiapp.api.data.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public record PublicDeckResponse(
        @JsonProperty("deck_id")
        Integer deckId,
        @JsonProperty("is_success")
        Boolean isSuccess
) implements Response {
}
