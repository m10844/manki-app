package ru.nsu.ccfit.mankiapp.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import ru.nsu.ccfit.mankiapp.api.data.mapper.StatusMapper;
import ru.nsu.ccfit.mankiapp.api.data.response.ErrorResponse;
import ru.nsu.ccfit.mankiapp.exceptions.BaseException;

@RestControllerAdvice
public class ExceptionTranslator extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = AccessDeniedException.class)
    protected ResponseEntity<Object> handleException(
            AccessDeniedException exception,
            WebRequest request
    ) {
        String bodyOfResponse;
        HttpStatus status;
        try {
            bodyOfResponse = new ObjectMapper().writeValueAsString(
                    new ErrorResponse(401, "Access denied")
            );
            status = HttpStatus.FORBIDDEN;
        } catch (JsonProcessingException e) {
            bodyOfResponse = "Unknown error";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return handleExceptionInternal(
                exception,
                bodyOfResponse,
                new HttpHeaders(),
                status,
                request
        );
    }

    @ExceptionHandler(value = BaseException.class)
    protected ResponseEntity<Object> handleException(BaseException exception, WebRequest request) {
        String bodyOfResponse;
        HttpStatus status = StatusMapper.mapStatus(exception);
        Integer statusValue = StatusMapper.mapStatusValue(exception);
        try {
            bodyOfResponse = new ObjectMapper().writeValueAsString(
                    new ErrorResponse(statusValue, exception.getMessage())
            );
        } catch (JsonProcessingException e) {
            bodyOfResponse = "Unknown error";
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return handleExceptionInternal(
                exception,
                bodyOfResponse,
                new HttpHeaders(),
                status,
                request
        );
    }
}
