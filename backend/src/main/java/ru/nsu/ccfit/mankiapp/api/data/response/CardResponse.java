package ru.nsu.ccfit.mankiapp.api.data.response;

import ru.nsu.ccfit.mankiapp.dto.CardDto;

public record CardResponse(CardDto card) implements Response {
}
