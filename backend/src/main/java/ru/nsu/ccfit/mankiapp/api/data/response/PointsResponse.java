package ru.nsu.ccfit.mankiapp.api.data.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public record PointsResponse(
        @JsonProperty("deck_id") Integer deckId,
        @JsonProperty("card_id") Integer cardId,
        @JsonProperty("points") Integer points) implements Response {
}
