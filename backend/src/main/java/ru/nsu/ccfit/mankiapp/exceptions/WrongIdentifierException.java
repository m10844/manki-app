package ru.nsu.ccfit.mankiapp.exceptions;

public class WrongIdentifierException extends BaseException{

    public WrongIdentifierException(String message) {
        super(message, ErrorType.WRONG_IDENTIFIER_ERROR);
    }
}
