package ru.nsu.ccfit.mankiapp.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.mankiapp.api.data.request.*;
import ru.nsu.ccfit.mankiapp.api.data.response.*;
import ru.nsu.ccfit.mankiapp.dto.*;
import ru.nsu.ccfit.mankiapp.dto.mapper.DeckMapper;
import ru.nsu.ccfit.mankiapp.services.DeckService;

import java.util.Collections;

@RestController
@Tag(name = "Deck", description = "All operations with deck")
@RequestMapping("/api/deck")
public class DeckController {

    private final DeckService deckService;

    public DeckController(DeckService deckService) {
        this.deckService = deckService;
    }

    @Operation(summary = "Get deck by id", tags = "deck")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Deck Found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = DeckResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "500",
                    description = "Internal server error",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    })
    })
    @GetMapping("/get/{id}")
    public Response getDeck(@PathVariable Integer id) {
        return new DeckResponse(deckService.getDeck(id));
    }

    @Operation(summary = "Modify already existing card", tags = {"deck", "card"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Deck after modifying card",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = DeckResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "500",
                    description = "Internal server error",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    })
    })
    @PostMapping(value = "/cards/edit", consumes = "application/json")
    public Response editCard(@RequestBody CardEditRequest cardEditRequest) {
        return new DeckResponse(
                deckService.modifyCards(
                        new CardWithDeckIdDto(
                                cardEditRequest.card(),
                                cardEditRequest.deckId()
                        )
                )
        );
    }

    @Operation(summary = "Add new card to the deck given", tags = {"deck", "card"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Card that has been added",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = CardResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "500",
                    description = "Internal server error",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    })
    })
    @PostMapping("/cards/add")
    public Response addCard(@RequestBody CardAddRequest cardAddRequest) {
        return new CardResponse(
                deckService.addCard(
                        DeckMapper.dtoFromCardAddRequest(cardAddRequest)
                )
        );
    }

    @Operation(summary = "Delete a list of cards from a deck with a given id", tags = {"deck", "card"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Deck after deleting cards",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = DeckResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "500",
                    description = "Internal server error",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    })
    })
    @DeleteMapping("/cards")
    public Response deleteCards(@RequestBody CardDeleteRequest cardDeleteRequest) {
        return new DeckResponse(
                deckService.deleteCards(
                        new CardsIdsWithDeckIdDto(
                                cardDeleteRequest.cardsIds(),
                                cardDeleteRequest.deckId()
                        )
                )
        );
    }

    @Operation(summary = "Add a new blank deck", tags = "deck")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Created deck",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = DeckResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "500",
                    description = "Internal server error",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    })
    })
    @PostMapping("/add")
    public Response addDeck(@RequestBody DeckAddRequest deckAddRequest) {
        return new DeckResponse(
                deckService.addDeck(
                        new DeckDto(0,
                                deckAddRequest.deckTitle(),
                                deckAddRequest.deckDescription(),
                                false,
                                Collections.emptyList())
                )
        );
    }

    @Operation(summary = "Delete deck with all its cards by id of the deck", tags = "deck")
    @DeleteMapping("/{id}")
    public void deleteDeck(@PathVariable Integer id) {
        deckService.deleteDeck(id);
    }

    @Operation(summary = "Get all decks")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Decks found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = AllDecksResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "500",
                    description = "Internal server error",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    })
    })
    @GetMapping("/all")
    public Response getAllDecks() {
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        return new AllDecksResponse(deckService.getAllDecks(name));
    }

    @Operation(summary = "Edit deck's title and description")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Deck after edition",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = DeckInfoResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "500",
                    description = "Internal server error",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    })
    })
    @PostMapping("/edit")
    public Response editDeck(@RequestBody DeckEditRequest deckEditRequest) {
        var result = deckService.editDeck(
                new DeckEditDto(
                        deckEditRequest.deckId(),
                        deckEditRequest.deckTitle(),
                        deckEditRequest.deckDescription())
        );
        return new DeckInfoResponse(result);
    }

    @Operation(summary = "Create token to share deck with given id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Deck id and share token",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = SharedDeckResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "500",
                    description = "Internal server error",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    })
    })
    @PostMapping("share/{id}")
    public Response shareDeck(@PathVariable Integer id) {
        var sharedDeck = deckService.shareDeck(id);
        return new SharedDeckResponse(sharedDeck.deckId(), sharedDeck.shareToken());
    }

    @Operation(summary = "Add deck to user's decks by share token")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Deck successfully added",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = DeckResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "500",
                    description = "Internal server error",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    })
    })
    @GetMapping("share/{share_id}")
    public Response getSharedDeck(@PathVariable("share_id") String shareId) {
        return new DeckResponse(deckService.getSharedDeck(shareId));
    }

    @Operation(summary = "Make deck public")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Request successfully processed. Check is_success field to get publishing result. True - deck is now public, false - deck is already public",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = PublicDeckResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "500",
                    description = "Internal server error",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "404",
                    description = "User doesn't have decks or wrong deckId. Check message to get reason",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    })
    })
    @PostMapping("/make_public/{deck_id}")
    public Response makePublic(@PathVariable("deck_id") Integer deckId) {
        var result = deckService.makeDeckPublic(deckId);
        return new PublicDeckResponse(deckId, result);
    }

    @Operation(summary = "Get all public decks")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Decks found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = AllDecksResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "500",
                    description = "Internal server error",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    })
    })
    @GetMapping("get_all_public")
    public Response getAllPublic() {
        var decks = deckService.getAllPublic();
        return new AllDecksResponse(decks);
    }
}
