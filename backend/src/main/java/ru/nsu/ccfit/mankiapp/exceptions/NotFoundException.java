package ru.nsu.ccfit.mankiapp.exceptions;

public class NotFoundException extends BaseException {
    public NotFoundException(String message) {
        super(message, ErrorType.NOT_FOUND);
    }
}
