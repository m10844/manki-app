package ru.nsu.ccfit.mankiapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.mankiapp.dto.ProfileDto;
import ru.nsu.ccfit.mankiapp.dto.mapper.UserMapper;
import ru.nsu.ccfit.mankiapp.exceptions.UserAlreadyExistsException;
import ru.nsu.ccfit.mankiapp.model.data.UserData;
import ru.nsu.ccfit.mankiapp.model.repo.security.UserRepository;

import javax.transaction.Transactional;


@Service
public class ProfileService {

    private final UserRepository userRepository;

    @Autowired
    public ProfileService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    public ProfileDto getProfileInfo() {
        return UserMapper.userDataToProfileDto(userData());
    }

    @Transactional
    public void updatePassword(String oldPassword, String newPassword) {
        var user = userData();
        if (!user.getUserPassword().equals(oldPassword)) {
            throw new UserAlreadyExistsException("Wrong password");
        }
        userRepository.updatePassword(user.getId(), newPassword);
    }

    private UserData userData() {
        var name = SecurityContextHolder.getContext().getAuthentication().getName();
        return userRepository
                .findByUserName(name)
                .orElseThrow(() -> new UserAlreadyExistsException("User with name " + name + " not found"));
    }
}
