package ru.nsu.ccfit.mankiapp.api.data.security;

import ru.nsu.ccfit.mankiapp.api.data.response.Response;

public record TokenRequestResponse(String token) implements Response {
}
