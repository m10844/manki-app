package ru.nsu.ccfit.mankiapp.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.mankiapp.api.data.response.ErrorResponse;
import ru.nsu.ccfit.mankiapp.api.data.response.Response;
import ru.nsu.ccfit.mankiapp.api.data.security.TokenRequestResponse;
import ru.nsu.ccfit.mankiapp.api.data.security.UserCredentialsRequest;
import ru.nsu.ccfit.mankiapp.dto.security.UserCredentials;
import ru.nsu.ccfit.mankiapp.services.security.UserSecurityService;

@RestController
@Tag(name = "Authentication", description = "All operations to authenticate user")
@RequestMapping("/auth")
public class UserCredentialsController {

    private final UserSecurityService userSecurityService;

    public UserCredentialsController(UserSecurityService userSecurityService) {
        this.userSecurityService = userSecurityService;
    }

    @Operation(summary = "Login into account", tags = "authentication")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Token to use after",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = TokenRequestResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "400",
                    description = "Error occurred",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    })
    })
    @PostMapping("/login")
    public Response login(@RequestBody UserCredentialsRequest request) {
        String token = userSecurityService.login(new UserCredentials(request.username(), request.password()));
        return new TokenRequestResponse(token);
    }

    @Operation(summary = "Register new account", tags = "authentication")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Token to use after",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = TokenRequestResponse.class))
                    }),
            @ApiResponse(
                    responseCode = "400",
                    description = "Error occurred",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorResponse.class))
                    })
    })
    @PostMapping("/register")
    public Response register(@RequestBody UserCredentialsRequest request) {
        String token = userSecurityService.register(new UserCredentials(request.username(), request.password()));
        return new TokenRequestResponse(token);
    }
}
