package ru.nsu.ccfit.mankiapp.dto;


public record TrainTimeDto(
        Integer deckId,
        Integer cardId,
        String userInput,
        Double timeTaken
) {
}
