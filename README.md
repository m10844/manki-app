# Manki App
### *Just like Anki but better*

**Manki app** is a brand new app that allows you to create custom memorize cards.
Just click "add new deck" and then type in everything you wanted to learn. After this, our app will help you to learn it all!

Manki has two types of decks: public and private. Public decks are visible to all users and training algorithm in them depends on how easy this card is for all the users on average. With private decks, algorithm adapts personally for you, and there are 3 different training modes available. If you still want to share deck with someone, you can easily get a sharing link by clicking on "share" button.

- Why is it better than Anki? 

Our app has a user friendly and intuitive interface:

[![photo-2022-06-01-17-09-39.jpg](https://i.postimg.cc/9XSksNGS/photo-2022-06-01-17-09-39.jpg)](https://postimg.cc/XXfxCL1c)

It has 4 training modes (while anki only has one):

[![photo-2022-06-01-17-13-15.jpg](https://i.postimg.cc/13DbGcVd/photo-2022-06-01-17-13-15.jpg)](https://postimg.cc/cv4DZ3m7)

In a Input Mode our system will check your answer automatically:

[![photo-2022-06-01-17-13-06.jpg](https://i.postimg.cc/mrwqqLwN/photo-2022-06-01-17-13-06.jpg)](https://postimg.cc/2q12LfgV)

It'll show you the right answer if you couldn't remember it

[![photo-2022-06-01-17-13-00.jpg](https://i.postimg.cc/J09dJGrm/photo-2022-06-01-17-13-00.jpg)](https://postimg.cc/MfmDJZfr)

And for training customization Manki will ask you how well you remembered it

[![photo-2022-06-01-17-13-03.jpg](https://i.postimg.cc/zBGMYs2m/photo-2022-06-01-17-13-03.jpg)](https://postimg.cc/SXHVWvpV)

But if you don't want to rate yourself, Maki can make it for you in Timed Mode. Timed Mode measures time taken for you to give the answer and decides how many points you should be given

[![photo-2022-06-01-17-12-56.jpg](https://i.postimg.cc/kGVpBxdf/photo-2022-06-01-17-12-56.jpg)](https://postimg.cc/2LDwtLyZ)
