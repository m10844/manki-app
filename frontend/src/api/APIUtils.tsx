import { API_BASE_URL, ACCESS_TOKEN } from "../config";
import axios, { AxiosError, AxiosResponse } from 'axios';
import { DeckSubmitInfo } from "../pages/add-new-deck-page/AddDeckPage";
import { MankiCard } from '../types/MankiDeck';
import { AnswerResult } from "../types/AnswerResult";

class RequestOptions {
    url: string;
    method: string;

    constructor(url: string, method: string) {
        this.url = url;
        this.method = method;
    }
};

export const http = axios.create({
    baseURL: API_BASE_URL,
    timeout: 20000
});

const request = (options: RequestOptions, data: any = undefined) => {
    if (options.method != 'POST' && data !== undefined) {
        throw new Error("Making not POST request with data");
    };

    const headers = new Headers({
        'Content-Type': 'application/json',
    });

    if (localStorage.getItem(ACCESS_TOKEN))
        headers.append('Authorization', `Bearer ${localStorage.getItem(ACCESS_TOKEN)}`);

    const defaults = { headers: headers };
    options = Object.assign({}, defaults, options);

    console.log(options)

    if (options.method == 'POST')
        return http.post(options.url, data, getUserTokenHeader()).then((resp: AxiosResponse) => resp.data).catch((reason: AxiosError) => console.error(reason.message));
    return http.get(options.url, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem(ACCESS_TOKEN)}`
        }
    })
        .then(
            // To specify the expected type: (response: AxiosResponse<{user:{name:string}}>
            (response: AxiosResponse) => {
                return response.data;
            }
        )
        .catch((reason: AxiosError) => {
            console.log(reason)
            console.log(reason.response);
            if (reason.response!.status === 400) {
                // TODO: Handle 400
            } else {
                // TODO: Handle else
            }
            console.error(reason.message);
        });
};

export function getSampleText() {
    return request(new RequestOptions('/some-mapping', 'GET'));
}

class MyRequestOptions {
    num: number;
};

export function myRequest(args: MyRequestOptions) {
    return request(new RequestOptions('/my-request', 'POST'), args);
}

class Creditinals {
    username: string
    password: string
};

export function login(args: Creditinals) {
    return request(new RequestOptions('/auth/login', 'POST'), args);
}

export function register(args: Creditinals) {
    return request(new RequestOptions('/auth/register', 'POST'), args);
}

export function addDeck(args: DeckSubmitInfo) {
    return request(new RequestOptions('/api/deck/add', 'POST'), args);
}

export function shareDeck(deckId: number) {
    return http.post(`/api/deck/share/${deckId}`, {}, getUserTokenHeader());
}

export function getSharedDeck(shareToken: string) {
    return http.get(`/api/deck/share/${shareToken}`, getUserTokenHeader());
}

// export function getDecks() {
//     return request(new RequestOptions('/api/deck/all', 'GET'));
// }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Тут отрефаченые запросы, если что, можно писать так же, но можно и не так же                                       //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function getProfile() {
    return request(new RequestOptions('api/profile/info', 'GET'));
}

const getUserTokenHeader = () => {
    return {
        headers: {
            Authorization: `Bearer ${localStorage.getItem(ACCESS_TOKEN)}`
        }
    }
}

export const getDecks = () =>
    http.get('/api/deck/all', getUserTokenHeader());

export const getPublicDecks = () => http.get('/api/deck/get_all_public', getUserTokenHeader());

export const getDeckById = (id: number) =>
    http.get(`/api/deck/get/${id}`, getUserTokenHeader());

export const modifyCard = (deckId: number, card: MankiCard) => {
    const data = {
        card: {
            id: card.id,
            question: card.question,
            answer: card.answer,
            question_image_path: "",
            answer_image_path: "",
            knowledge_points: card.knowledge_points
        },
        deck_id: deckId
    }
    return http.post(`/api/deck/cards/edit`, data, getUserTokenHeader())
}

export const addCard = (deckId: number, newCard: MankiCard) => {
    const data = {
        question: newCard.question,
        answer: newCard.answer,
        deckId: deckId,
        question_image_path: "",
        answer_image_path: ""
    }
    return http.post(`/api/deck/cards/add`, data, getUserTokenHeader())
}

export const changePassword = (old_pass: string, new_pass: string) => {
    const data = {
        old_password: old_pass,
        new_password: new_pass
    };
    return http.post('/api/profile/change_password', data, getUserTokenHeader());
}

export const deleteDeck = (id: number) =>
    http.delete(`/api/deck/${id}`, getUserTokenHeader());

export const deleteCards = (deckId: number, cardIds: number[]) => {
    const data = {
        deck_id: deckId,
        cards_ids: cardIds
    }

    return http.delete(`/api/deck/cards`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem(ACCESS_TOKEN)}`
        },
        data: data
    });

}

export const submitAnkiAnsw = (deckId: number, cardId: number, answer: AnswerResult) => {
    const data = {
        deck_id: deckId,
        card_id: cardId,
        answer: answer
    }
    return http.post(`/api/train/anki`, data, getUserTokenHeader())
}

export const submitStatAnsw = (deckId: number, cardId: number, answer: AnswerResult) => {
    const data = {
        deck_id: deckId,
        card_id : cardId,
        answer : answer
    }
    return http.post(`/api/train/integral`, data, getUserTokenHeader())
}

export function makeDeckPublic(deckId: number) {
    return http.post(`/api/deck/make_public/${deckId}`, {}, getUserTokenHeader());
}
