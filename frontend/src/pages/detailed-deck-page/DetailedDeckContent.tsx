import { Button, Card, CardContent, Divider, Grid, makeStyles, Typography } from '@mui/material';
import React from 'react'
import { useNavigate } from 'react-router-dom';
import { Container } from 'reactstrap';
import { makeDeckPublic } from '../../api/APIUtils';
import paths from '../../routing/routes';
import { MankiDeck } from '../../types/MankiDeck';



export const DetailedDeckContent: React.FC<MankiDeck> = ({ deck }) => {
    const navigate = useNavigate();

    const getColor = (points: number) => {
        if (points < 4){
            return '#a73a38'
        }else if (points < 8){
            return '#ff7043'
        } else if (points < 12) {
            return '#ffb300'
        } else if (points >= 12){
            return '#689f38'
        }
    }

    return (
        <Container container
            alignItems="center"
            justifyContent="center"
            style={{ minHeight: '100vh' }} >

            <Button 
                variant={`outlined`} 
                sx={{ m: 2 }} 
                onClick={() => {
                     navigate(`/${paths.DECK_LIST}`) 
                     }}>
                Back
            </Button>

            <Button 
                variant={`outlined`} 
                sx={{ m: 2 }} 
                onClick={() => {
                    if (deck.is_public){
                        navigate(`${paths.TRAIN}/../../stat_train`)
                    } else {
                        navigate(`/deck/${deck.id}/${paths.CHOOSE_TRAIN_MODE}`) 
                    }
                     }}>
                Train
            </Button>

            <Button 
                variant={`outlined`} 
                sx={{ m: 2 }} 
                onClick={() => {
                    makeDeckPublic(deck.id) 
                    alert("Deck is now public!")
                     }}>
                Make Public
            </Button>

            <Typography sx={{ p: 2, m: 2, textAlign: 'center', minWidth: '100%', color:"secondary.dark" }} variant="h2">{deck.deck_title}</Typography>
            
            {[deck.cards.map(
                (card) =>
                    <Card sx={{ alignSelf: 'center', m: 2, boxShadow: "1px 5px 5px 5px #aaaaaa"}} key={card.id.toString()}>
                        <CardContent>
                            <Grid container spacing={2}>

                                <Grid item xs={12}>
                                <Typography sx={{ fontSize: 12, textAlign: "right" }} color="text.secondary" gutterBottom>
                                    Knowlege points: {card.knowledge_points}
                                </Typography>
                                </Grid>

                            </Grid>

                            <Typography sx={{ textAlign: "center" }} color={getColor(card.knowledge_points)} variant="h5" component="div" gutterBottom>
                                {card.question}
                            </Typography>

                            <Divider />

                            <Typography sx={{ textAlign: "center", mt: 1 }} color={getColor(card.knowledge_points)} variant="h5" component="div">
                                {card.answer}
                            </Typography>
                        </CardContent>
                    </Card>
            )]}

        </Container>
    );
}