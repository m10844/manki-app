import { LinearProgress, Typography } from '@mui/material';
import React from 'react';
import { useDeck } from '../../components/DeckLoaderComponent';
import { LoadingState } from '../../types/LoadingState';
import { DetailedDeckContent } from './DetailedDeckContent';


export const DetailedDeckPage: React.FC = () => {
    const deckWrapper = useDeck();

    switch (deckWrapper.state) {
        case LoadingState.LOADING: {
            return (<LinearProgress sx={{ m: 2 }} />);
        }
        case LoadingState.LOADED: {
            console.log(deckWrapper.result.deck);
            return (<DetailedDeckContent deck={deckWrapper.result.deck} />);
        }
        case LoadingState.FAILED: {
            return (<Typography variant="h6">Failed to load content</Typography>);
        }

    }
}