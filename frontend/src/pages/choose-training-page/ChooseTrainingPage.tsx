import { FormatBold } from '@mui/icons-material';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Box from '@mui/system/Box/Box';
import * as React from 'react';
import { useNavigate } from 'react-router-dom';
import paths from '../../routing/routes';


const ChooseTrainingPage: React.FC = () => {
    
    const navigate = useNavigate();


    return (
        <Box sx={{ p: 3 }}>
            <Typography variant="h5" color="primary" sx={{ textTransform: 'capitalize', fontWeight: 'bold' }}>
                Choose training type:
            </Typography>
            
            <Box sx={{ p: 4 }}>
                <Grid
                    container
                    spacing={{ xs: 2, md: 2 }}
                    columns={{ xs: 2, sm: 4, md: 6 }}>

                        <Grid item xs={2} sm={4} md={2} key={1}>
                            <Button fullWidth variant="contained" color="success" onClick={() => { navigate(`${paths.TRAIN}/../../train`)}}>
                                Standart Anki
                            </Button>
                        </Grid>

                        <Grid item xs={2} sm={4} md={2} key={1}>
                            <Button fullWidth variant="contained" color="info" onClick={() => { navigate(`${paths.TRAIN}/../../input_train`) }}>
                                Input Anki
                            </Button>
                        </Grid>

                        <Grid item xs={2} sm={4} md={2} key={1}>
                            <Button fullWidth variant="contained" color="warning" onClick={() => { navigate(`${paths.TRAIN}/../../timed_train`) }}>
                                Timed Anki
                            </Button>
                        </Grid>

                </Grid>
            </Box>
               
            <Typography  component="div" gutterBottom sx={{ p: 1 }}>
            <Typography display='inline' sx={{ color: 'success.main', textTransform: 'capitalize', fontWeight: 'bold' }}>STANDART ANKI </Typography>
            —  Mode in which you are to choosef how well you actually remeber the card. Stay honest with yourself ;)
            </Typography>

            <Typography  component="div" gutterBottom sx={{ p: 1 }}>
            <Typography display='inline' sx={{ color: 'info.main', textTransform: 'capitalize', fontWeight: 'bold' }}>INPUT ANKI </Typography>
            —  Type in your answer and MankiApp will automatically check it. If you were right, it's up to you to decide how well you know this card
            </Typography>

            <Typography  component="div" gutterBottom sx={{ p: 1 }}>
            <Typography display='inline' sx={{ color: 'warning.main', textTransform: 'capitalize', fontWeight: 'bold' }}>TIMED ANKI </Typography>
            —  In this mode Manki App will measure time taken for you to answer and rate you based on that
            </Typography>
            
        </Box>
    );
}

export default ChooseTrainingPage