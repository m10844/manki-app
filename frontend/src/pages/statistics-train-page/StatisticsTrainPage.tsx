import * as React from "react";
import StatisticsTrainCardComponent from "./StatisticsTrainCardComponent";
import { useDeck } from "../../components/DeckLoaderComponent";
import { LoadingState } from "../../types/LoadingState";
import { Box, LinearProgress, Typography } from "@mui/material";
import { LoadingFailedComponent } from "../../components/LoadingFailedComponent";

const StatisticsTrainPage: React.FC = () => {

    const deckWrapper = useDeck();
    const [currCardIdx, setCurrCardIdx] = React.useState<number>(0);

    console.log("card-list")
    console.log("deckWrapper.result.deck", deckWrapper.result)

    const onCardFinished = () => {
        console.log("card finished")
        if (currCardIdx + 1 < deckWrapper.result.deck.cards.length) {
            setCurrCardIdx(currCardIdx + 1);
        } else {
            setCurrCardIdx(0);
        }
    };  

    switch (deckWrapper.state) {
        case LoadingState.LOADING: {
            return (<LinearProgress sx={{ m: 2 }} />);
        }
        case LoadingState.LOADED: {
            if (deckWrapper.result.deck.cards.length == 0) {
                return (
                    <Box sx={{ display: `flex`, flexDirection: `column`, justifyContent: `center`, height: `350px` }}>
                        <Typography textAlign={`center`} variant="h3">Empty deck</Typography>
                    </Box>
                );
            } else {
                return (
                    <StatisticsTrainCardComponent
                        deck_id={deckWrapper.result.deck.id}
                        card_id={deckWrapper.result.deck.cards[currCardIdx].id}
                        deckName={deckWrapper.result.deck.deck_title}
                        answer={deckWrapper.result.deck.cards[currCardIdx].answer}
                        question={deckWrapper.result.deck.cards[currCardIdx].question}
                        onFinished={onCardFinished} 
                        knowledge_points={deckWrapper.result.deck.cards[currCardIdx].knowledge_points}
                        />
                );
            }
        }
        case LoadingState.FAILED: {
            return (<LoadingFailedComponent reason={deckWrapper.reason} />);
        }
    }
}

export default StatisticsTrainPage