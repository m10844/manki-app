import * as React from "react";
import { useState } from "react";
import CardQuestionPage from '../train-page/CardQuestionPage';
import CardAnswerPage from '../train-page/CardAnswerPage';
import { AnswerResult } from "../../types/AnswerResult";
import { submitStatAnsw } from "../../api/APIUtils";

interface CardTrainProps {
    deck_id: number,
    card_id : number,
    deckName: string,
    question: string,
    answer: string,
    knowledge_points: number,
    onFinished: () => void,
}

const StatisticsTrainCardComponent: React.FC<CardTrainProps> = ({ deck_id, card_id, deckName, question, answer, knowledge_points, onFinished }) => {
    const [showAnswer, setShowAnswer] = useState<boolean>(false);

    const onCardFinished = (answer: AnswerResult) => {
        submitStatAnsw(deck_id, card_id, answer);
        console.log(answer);
        setShowAnswer(false);
        onFinished();
    }

    const questionView = (<CardQuestionPage question={question} onFlip={() => setShowAnswer(true)} deckName={deckName} knowledge_points={knowledge_points} />);
    const answerView = (<CardAnswerPage answer={answer} deckName={deckName} onAnswer={onCardFinished} knowledge_points={knowledge_points}/>);

    if (showAnswer) {
        return answerView
    } else {
        return questionView
    }

}

export default StatisticsTrainCardComponent