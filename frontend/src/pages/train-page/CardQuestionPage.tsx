import * as React from "react";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import MankiCard from "../../components/Cards/MankiCard";

interface CardQuestionProps {
    question: string,
    deckName: string,
    onFlip: () => void,
    knowledge_points: number
}

const CardQuestionPage: React.FC<CardQuestionProps> = ({ knowledge_points, question: text, deckName, onFlip }) => {
    return (
        <Box sx={{ p: 10 }}>
            <MankiCard bodyText={text} deckName={deckName} knowledgePoints={knowledge_points}/>
            <br />
            <Button fullWidth variant="contained" color="primary" onClick={onFlip} >Flip</Button>
        </Box>
    )
}

export default CardQuestionPage
