import React from 'react';
import TrainButtonsComponent from "../../components/TrainButtonsComponent";
import MankiCard, { CardInfo } from "../../components/Cards/MankiCard";
import Box from "@mui/material/Box";
import { AnswerResult } from "../../types/AnswerResult";

interface CardAnswerProps {
    answer: string;
    deckName: string;
    onAnswer: (answer: AnswerResult) => void;
    enableFail?: boolean;
    knowledge_points: number;
}

const CardAnswerPage: React.FC<CardAnswerProps> = (props) => {
    return (
        <Box sx={{ p: 10 }}>
            <MankiCard bodyText={props.answer} deckName={props.deckName} knowledgePoints={props.knowledge_points} />
            <br />
            <TrainButtonsComponent onAnswer={props.onAnswer} enableFail={props.enableFail} />
        </Box>
    )
}

CardAnswerPage.defaultProps = {
    enableFail: true
};

export default CardAnswerPage
