import * as React from "react";
import { useState } from "react";

import { AnswerResult } from "../../types/AnswerResult";
import { SubmitHandler, useForm, Controller } from "react-hook-form";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import MankiCard from "../../components/Cards/MankiCard";
import Button from "@mui/material/Button";
import ShortTrainButtonsComponent from "../../components/ShortTrainButtonsComponent";
import WhenFailedButtonComponent from "../../components/WhenFailedButtonComponent";
import { submitAnkiAnsw } from "../../api/APIUtils";
import { Typography } from "@mui/material";
import { TimerComponent } from "../../components/TimerComponent";

export interface Answer {
    submmitted_answer: string
}

interface CardTrainProps {
    deck_id: number,
    card_id : number,
    deckName: string,
    question: string,
    answer: string,
    knowledge_points: number,
    onFinished: () => void,
}


const TimedTrainCardComponent: React.FC<CardTrainProps> = ({ deck_id, card_id, deckName, question, answer, knowledge_points, onFinished }) => {
    const [showAnswer, setShowAnswer] = useState<boolean>(false);
    const [answerIsRight, setAnswer] = useState<boolean>(false);

    const [startTime] = useState<number>(Date.now())
    const [endTime, setEndTime] = useState<number>(startTime)


    const onCardFinished = () => {
        let ans = AnswerResult.FAIL
        const delta = ((endTime - startTime) / 1000) - 2
        if (answerIsRight) {
            if (delta > answer.length) {
                console.log('good')
                ans = AnswerResult.GOOD
            } else {
                console.log('easy')
                ans = AnswerResult.EASY
            }

        }
        submitAnkiAnsw(deck_id, card_id, ans);
        setShowAnswer(false);
        onFinished();
    }

    const {
        control,
        handleSubmit,
        reset
    } = useForm<Answer>({
        defaultValues: {
            submmitted_answer: "",
        }
    });

    const onSubmit: SubmitHandler<Answer> = (data) => {
        if (data.submmitted_answer == answer) {
            setShowAnswer(true);
            setAnswer(true);
        } else {
            setShowAnswer(true);
            setAnswer(false);
        }
        setEndTime(Date.now());
        reset({ submmitted_answer: "" })
    }

    const questionView = (
        <Box sx={{ p: 10 }}>
            <TimerComponent />
            <MankiCard bodyText={question} deckName={deckName} knowledgePoints={knowledge_points}/>
            <Controller
                name="submmitted_answer"
                control={control}
                rules={{
                    required: {
                        value: true,
                        message: "Required field"
                    }
                }}
                render={({ field }) => (
                    <TextField
                        {...field}
                        label="Your answer"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                    />
                )}
            />
            <Button variant="contained" sx={{ mt: 1 }} onClick={handleSubmit(onSubmit)} fullWidth>Check my answer</Button>
        </Box>
    );

    const rightAnswerView = (
        <Box sx={{ p: 10 }}>
            <MankiCard bodyText={"Your answer is right"} deckName={deckName} knowledgePoints={knowledge_points}/>
            <Button fullWidth variant="contained" color="primary" onClick={() => { onCardFinished() }}>NEXT</Button>
        </Box>
    );

    const wrongAnswerView = (
        <Box sx={{ p: 10 }}>
            <MankiCard bodyText={`Your answer is wrong. The right one is: ${answer}`} deckName={deckName} knowledgePoints={knowledge_points}/>
            <Button fullWidth variant="contained" color="error" onClick={() => { onCardFinished() }}>NEXT</Button>
        </Box>
    );

    if (!showAnswer) {
        return questionView
    } else if (answerIsRight) {
        return rightAnswerView
    } else {
        return wrongAnswerView
    }

}

export default TimedTrainCardComponent