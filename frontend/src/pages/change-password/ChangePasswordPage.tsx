import { Button, TextField, Typography } from '@mui/material';
import { Box } from '@mui/system';
import React, { useState } from 'react';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { changePassword } from '../../api/APIUtils';

interface PassChangeData {
  old_password: string,
  new_password: string
}

const ChangePasswordPage: React.FC = () => {
  const navigate = useNavigate();
  const [errorMessage, seterrorMessage] = useState<string>("")
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<PassChangeData>({
    defaultValues: {
      old_password: "",
      new_password: ""
    }
  });


  const onSubmit: SubmitHandler<PassChangeData> = (data) => {
    changePassword(data.old_password, data.new_password).then(r => {
      if (r.status !== 200)
        seterrorMessage(r.data.message);
    })
  }

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
      <Box sx={{ width: 1 / 4, alignSelf: 'center' }}>
        <form>
          <Typography variant={'caption'} textAlign={'center'} color={'red'}>{errorMessage}</Typography>
          <Controller
            name="old_password"
            control={control}
            rules={{
              required: {
                value: true,
                message: "Required field"
              }
            }}
            render={({ field }) => (
              <TextField
                {...field}
                label="Old Password"
                variant="outlined"
                error={!!errors.old_password}
                helperText={errors.old_password ? errors.old_password?.message : ''}
                fullWidth
                margin="dense"
              />
            )}
          />

          <Controller
            name="new_password"
            control={control}
            rules={{
              required: {
                value: true,
                message: "Required field"
              }
            }}
            render={({ field }) => (
              <TextField
                {...field}
                type="password"
                label="New Password"
                variant="outlined"
                error={!!errors.new_password}
                helperText={errors.new_password ? errors.new_password?.message : ''}
                fullWidth
                margin="dense"
              />
            )}
          />

          <Button variant="contained" sx={{ mt: 1 }} onClick={handleSubmit(onSubmit)} fullWidth>Change</Button>
        </form>
      </Box>
    </Box>
  )
}

export default ChangePasswordPage;
