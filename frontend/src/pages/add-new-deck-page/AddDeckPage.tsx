import { Box, Button, Checkbox, FormControlLabel, TextField, Typography } from '@mui/material';
import React, { useState } from 'react';
import { useForm, SubmitHandler, Controller } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { addDeck, makeDeckPublic } from '../../api/APIUtils';

export interface DeckSubmitInfo {
    deck_title: string,
    deck_description: string
}

export const AddDeckPage: React.FC = () => {
    const navigate = useNavigate();
    const [errorMessage, seterrorMessage] = useState<string>("")

    const {
        control,
        handleSubmit,
    } = useForm<DeckSubmitInfo>({
        defaultValues: {
            deck_title: "",
            deck_description: ""
        }
    });


    const onSubmit: SubmitHandler<DeckSubmitInfo> = (data) => {
        addDeck({ deck_title: data.deck_title, deck_description: data.deck_description }).then(data => {
            if (checked){
                makeDeckPublic(data.deck.id)
            }
            navigate(`/deck/${data.deck.id}/edit`);
        }
        )
    }

    const [checked, setChecked] = React.useState(true);

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setChecked(event.target.checked);
    };
  

    return (
        <Box sx ={{ p : 8 }}>
            <form>
                <Typography variant={'caption'} textAlign={'center'} color={'red'}>{errorMessage}</Typography>

                <Controller
                    name="deck_title"
                    control={control}
                    rules={{
                        required: {
                            value: true,
                            message: "Required field"
                        }
                    }}
                    render={({ field }) => (
                        <TextField
                            {...field}
                            label="Deck name"
                            variant="outlined"
                            fullWidth
                            margin="dense"
                        />
                    )}
                />

                <Controller
                    name="deck_description"
                    control={control}
                    rules={{
                        required: {
                            value: true,
                            message: "Required field"
                        }
                    }}
                    render={({ field }) => (
                        <TextField
                            {...field}
                            label="Deck description"
                            variant="outlined"
                            fullWidth
                            margin="dense"
                        />
                    )}
                />

            <FormControlLabel
                label="Public Deck"
                control={<Checkbox checked={checked} onChange={handleChange} />}
            />

                <Button variant="contained" sx={{ mt: 1 }} onClick={handleSubmit(onSubmit)} fullWidth>Create new deck</Button>
            </form>
        </Box>
    )
}
