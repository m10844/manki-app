import { Box } from '@mui/material';
import React from 'react';
import { DeckListPreview } from '../../components/DeckListPreview';
import { PublicDecksList } from '../../components/PublicDecksList';
import UserInfoComponent from '../../components/UserInfoComponent';


const HomePage: React.FC = () => {

    return (
        <Box sx={{ flexGrow: 1 }}>
            <UserInfoComponent />
            <Box sx={{ mb: 4, ml: 2, mr: 2 }}>
                <DeckListPreview />
            </Box>
            <Box sx={{ mb: 4, ml: 2, mr: 2 }}>
                <PublicDecksList />
            </Box>
        </Box>
    )
}

export default HomePage

