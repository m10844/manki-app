import { Card, CardContent, TextField, Divider, Typography, Button, CircularProgress, LinearProgress } from '@mui/material';
import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';
import { MankiCard } from '../../types/MankiDeck';
import { SubmitHandler, Controller } from 'react-hook-form';
import { LoadingWrapper } from '../../types/LoadingWrapper';
import { LoadingState } from '../../types/LoadingState';
import { LoadingFailedComponent } from '../../components/LoadingFailedComponent';
import { string } from 'yup';

interface EditCardProps {
    card: MankiCard,
    onModifyCard: (mankiCard: MankiCard) => void,
    onRemoveCard: (mankiCard: MankiCard) => void,
    onContentChanged: (mankiCard: MankiCard) => void
}

interface ModifyingProps {
    question: string,
    answer: string
}


export const EditCardComponent: React.FC<EditCardProps> = (
    {
        card,
        onModifyCard,
        onRemoveCard,
        onContentChanged
    }
) => {
    const [errorMessage, seterrorMessage] = useState<string>("")
    const [state, setState] = useState<LoadingState>(LoadingState.LOADED)
    const {
        control,
        handleSubmit,
        watch,
        formState: { errors },
    } = useForm<ModifyingProps>({
        defaultValues: {
            question: card.question,
            answer: card.answer
        }
    });

    watch((value, { name, type }) => {
        onContentChanged({
            id: card.id,
            answer: value.answer,
            question: value.question,
            knowledge_points: card.knowledge_points
        })
    })

    const onSubmit: SubmitHandler<ModifyingProps> = (data) => {
        onModifyCard({ id: card.id, question: data.question, answer: data.answer, knowledge_points: 0 })
    }

    switch (state) {
        case LoadingState.LOADING: {
            return (<LinearProgress sx={{ m: 2 }} />);
        }
        case LoadingState.LOADED || LoadingState.FAILED: {
            return (<Card sx={{ alignSelf: 'center', m: 2 }}>
                <CardContent>
                    <form>
                        <Typography variant={'caption'} textAlign={'center'} color={'red'}>{errorMessage}</Typography>
                        <Controller
                            name="question"
                            control={control}
                            rules={{
                                required: {
                                    value: true,
                                    message: "Required field"
                                }
                            }}
                            render={({ field }) => (
                                <TextField
                                    {...field}
                                    label="Question"
                                    variant="outlined"
                                    error={!!errors.question}
                                    helperText={errors.question ? errors.question?.message : ''}
                                    fullWidth
                                    margin="dense"
                                />
                            )}
                        />

                        <Controller
                            name="answer"
                            control={control}
                            rules={{
                                required: {
                                    value: true,
                                    message: "Required field"
                                }
                            }}
                            render={({ field }) => (
                                <TextField
                                    {...field}
                                    label="Answer"
                                    variant="outlined"
                                    error={!!errors.answer}
                                    helperText={errors.answer ? errors.answer?.message : ''}
                                    fullWidth
                                    margin="dense"
                                />
                            )}
                        />
                        <Button variant="outlined" sx={{ mr: 1 }} color={`error`} onClick={() => { onRemoveCard(card) }}>Delete</Button>
                        <Button variant="contained" sx={{ m: 1 }} onClick={handleSubmit(onSubmit)} >Save</Button>
                    </form>
                </CardContent>
            </Card>);
        }
    }
}