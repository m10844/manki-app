import { Satellite } from '@mui/icons-material';
import { Button, Container, LinearProgress, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { convertCompilerOptionsFromJson } from 'typescript';
import { addCard, modifyCard, deleteCards, deleteDeck, getDeckById } from '../../api/APIUtils';
import DeckDetailsCard from '../../components/Cards/DeckDetailsCard';
import { useDeck } from '../../components/DeckLoaderComponent';
import { LoadingFailedComponent } from '../../components/LoadingFailedComponent';
import paths from '../../routing/routes';
import { LoadingState } from '../../types/LoadingState';
import { LoadingWrapper } from '../../types/LoadingWrapper';
import { MankiCard, MankiDeck } from '../../types/MankiDeck';
import { EditCardComponent } from './EditCardComponent';


export const EditDeckPage: React.FC = () => {

    const [deckEdited, setEditedDeck] = useState<LoadingWrapper<MankiDeck>>({ state: LoadingState.LOADING, result: null, reason: null });
    const { deckId } = useParams();
    const [id] = useState<number>(Number(deckId));

    useEffect(() => {
        getDeckById(id).then(response => {
            if (response.status == 200) {
                setEditedDeck({ state: LoadingState.LOADED, result: response.data, reason: null });
                console.log(response.data.deck)
            }
        }).catch(
            error => {
                if (error.response.status == 403) {
                    setEditedDeck({ state: LoadingState.FAILED, result: null, reason: "Access denied" });
                } else {
                    setEditedDeck({ state: LoadingState.FAILED, result: null, reason: null });
                }

            }
        );
    }, [id]);

    const navigate = useNavigate();

    const onCardContentChanged = (card: MankiCard) => {
        const idx = deckEdited.result.deck.cards.findIndex(item => item.id == card.id)
        if (idx != -1) {
            deckEdited.result.deck.cards[idx] = card
        }
    }

    const onAddCard = () => {
        console.log(`Adding new card`)

        setEditedDeck({
            state: LoadingState.LOADING,
            result: deckEdited.result,
            reason: null
        })

        addCard(deckEdited.result.deck.id, {
            id: 0,
            question: "Placeholder",
            answer: "Placeholder",
            knowledge_points: 0
        }).then(
            response => {
                deckEdited.result.deck.cards.push({
                    id: response.data.card.id,
                    question: response.data.card.question,
                    answer: response.data.card.answer,
                    knowledge_points: response.data.card.knowledge_points
                })
                setEditedDeck({
                    state: LoadingState.LOADED,
                    result: deckEdited.result,
                    reason: null
                })
            }
        ).catch(
            error => {
                if (error.response.status == 403) {
                    setEditedDeck({ state: LoadingState.FAILED, result: deckEdited.result, reason: "Access denied" });
                } else {
                    setEditedDeck({ state: LoadingState.FAILED, result: deckEdited.result, reason: "Failed to add new card" });
                }
            }
        )
    }

    const onModifyCard = (modifiedCard: MankiCard): void => {
        setEditedDeck({
            state: LoadingState.LOADING,
            result: deckEdited.result,
            reason: null
        })

        modifyCard(deckEdited.result.deck.id, {
            id: modifiedCard.id,
            question: modifiedCard.question,
            answer: modifiedCard.answer,
            knowledge_points: modifiedCard.knowledge_points
        }).then(
            response => {
                if (response.status == 200) {
                    setEditedDeck({
                        state: LoadingState.LOADED,
                        result: deckEdited.result,
                        reason: null
                    })
                } else {
                    console.log("ILLEGAL STATE ON MODIFY")
                }
            }
        ).catch(
            error => {
                if (error.response.status == 403) {
                    setEditedDeck({ state: LoadingState.FAILED, result: deckEdited.result, reason: "Access denied" });
                } else {
                    setEditedDeck({ state: LoadingState.FAILED, result: deckEdited.result, reason: "Failed to modify card" });
                }
            }
        )
    }


    const onRemoveCard = (cardToRemove: MankiCard): void => {
        setEditedDeck({
            state: LoadingState.LOADING,
            result: deckEdited.result,
            reason: null
        })

        deleteCards(deckEdited.result.deck.id, [cardToRemove.id]).then(
            response => {
                if (response.status == 200) {
                    deckEdited.result.deck.cards = deckEdited.result.deck.cards.filter(card => card.id != cardToRemove.id)
                    setEditedDeck({
                        state: LoadingState.LOADED,
                        result: deckEdited.result,
                        reason: null
                    })
                } else {
                    console.log("UNDEFINED STATE ON DELETE")
                }
            }
        ).catch(
            error => {
                if (error.response.status == 403) {
                    setEditedDeck({ state: LoadingState.FAILED, result: deckEdited.result, reason: "Access denied" });
                } else {
                    setEditedDeck({ state: LoadingState.FAILED, result: deckEdited.result, reason: "Failed to remove card" });
                }
            }
        )
    }

    switch (deckEdited.state) {
        case LoadingState.LOADING: {
            return (<LinearProgress sx={{ m: 2 }} />);
        }
        case LoadingState.LOADED: {
            return (
                <Container >
                    <Button variant={`outlined`} sx={{ m: 2 }} onClick={() => { navigate(`/${paths.DECK_LIST}`) }}>Back</Button>
                    <Typography sx={{ m: 2, textAlign: 'center', minWidth: '100%' }} variant="h2">{deckEdited.result.deck.deck_title}</Typography>

                    {[deckEdited.result.deck.cards.map(
                        (card) => <EditCardComponent
                            card={card}
                            onModifyCard={onModifyCard}
                            onRemoveCard={onRemoveCard}
                            onContentChanged={onCardContentChanged}
                            key={card.id.toString()} />
                    )]}
                    <Button variant={`outlined`} sx={{ m: 2 }} onClick={() => onAddCard()} >Add card</Button>
                </Container>
            );
        }
        case LoadingState.FAILED: {
            return (<LoadingFailedComponent reason={deckEdited.reason} />);
        }
    }
}