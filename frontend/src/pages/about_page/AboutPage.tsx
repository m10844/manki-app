import { FormatBold } from '@mui/icons-material';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Box from '@mui/system/Box/Box';
import * as React from 'react';
import { useNavigate } from 'react-router-dom';

const AboutPage: React.FC = () => {
    
    const navigate = useNavigate();


    return (
        <Box sx={{ p: 3 }}>
            <Typography variant="h5" color="primary" sx={{ textTransform: 'capitalize', fontWeight: 'bold' }}>
                We have recently released 4 different training modes:
            </Typography>
               
            <Box sx={{ mt: 3, mb: 3 }}>
                <Typography  component="div" gutterBottom sx={{ p: 1 }}>
                <Typography display='inline' sx={{ color: 'success.main', textTransform: 'capitalize', fontWeight: 'bold' }}>STANDART ANKI </Typography>
                —  Mode in which you are to choosef how well you actually remeber the card. Stay honest with yourself ;)
                </Typography>

                <Typography  component="div" gutterBottom sx={{ p: 1 }}>
                <Typography display='inline' sx={{ color: 'info.main', textTransform: 'capitalize', fontWeight: 'bold' }}>INPUT ANKI </Typography>
                —  Type in your answer and MankiApp will automatically check it. If you were right, it's up to you to decide how well you know this card
                </Typography>

                <Typography  component="div" gutterBottom sx={{ p: 1 }}>
                <Typography display='inline' sx={{ color: 'warning.main', textTransform: 'capitalize', fontWeight: 'bold' }}>TIMED ANKI </Typography>
                —  In this mode Manki App will measure time taken for you to answer and rate you based on that
                </Typography>

                <Typography  component="div" gutterBottom sx={{ p: 1 }}>
                <Typography display='inline' sx={{ color: 'error.main', textTransform: 'capitalize', fontWeight: 'bold' }}>STATISTICS BASED </Typography>
                —  This is a special mode for <strong>public</strong> decks in which Manki app finds out how easy this card is based on other people's experience
                </Typography>
            </Box>

            <Button fullWidth variant="contained" color="secondary" onClick={() => { navigate('/deck-list') }}>
                Start training!
            </Button>
            
        </Box>

    );
}

export default AboutPage