import { Box, Container, Typography } from '@mui/material';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import paths from '../../routing/routes';

const REDIRECT_TIMEOUT = 5000

export const NotFoundPage: React.FC = () => {
    const navigate = useNavigate();
    const redirect = () => {
        navigate(paths.HOME);
    }

    setTimeout(() => {
        console.log('nav')
        redirect();
    }, REDIRECT_TIMEOUT);

    return (
        <Box sx={{ display: `flex`, flexDirection: `column`, justifyContent: `center`, height: `600px` }}>
            <Typography textAlign={`center`} variant="h1">404</Typography>
            <Typography textAlign={`center`} variant="h2">Page not found</Typography>
            <Typography textAlign={`center`} variant="h5">You will be redirected in 5 seconds...</Typography>
        </Box>
    );
}