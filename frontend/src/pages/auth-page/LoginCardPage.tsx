
import { Button, TextField, Typography } from '@mui/material';
import React, { useState } from 'react';
import { useForm, SubmitHandler, Controller } from 'react-hook-form';
import { login } from '../../api/APIUtils';
import { saveToken } from '../../utils/creditinalsUtils';
import { useNavigate } from 'react-router-dom';
import paths from '../../routing/routes';

interface LoginData {
    name: string,
    password: string
}

const LoginCardPage: React.FC = () => {
    const navigate = useNavigate();
    const [errorMessage, seterrorMessage] = useState<string>("")
    const {
        control,
        handleSubmit,
        formState: { errors },
    } = useForm<LoginData>({
        defaultValues: {
            name: "",
            password: ""
        }
    });


    const onSubmit: SubmitHandler<LoginData> = (data) => {
        login({ username: data.name, password: data.password }).then(data => {
            if (data.token) {
                saveToken(data.token) 
                navigate(paths.HOME)
                window.location.reload()
            } else {
                seterrorMessage(data.message)
            }
        }
        )
    }

    return (
        <form>
            <Typography variant={'caption'} textAlign={'center'} color={'red'}>{errorMessage}</Typography>
            <Controller
                name="name"
                control={control}
                rules={{
                    required: {
                        value: true,
                        message: "Required field"
                    }
                }}  
                render={({ field }) => (
                    <TextField
                        {...field}
                        label="Name"
                        variant="outlined"
                        error={!!errors.name}
                        helperText={errors.name ? errors.name?.message : ''}
                        fullWidth
                        margin="dense"
                    />
                )}
            />

            <Controller
                name="password"
                control={control}
                rules={{
                    required: {
                        value: true,
                        message: "Required field"
                    }
                }}
                render={({ field }) => (
                    <TextField
                        {...field}
                        type="password"
                        label="Password"
                        variant="outlined"
                        error={!!errors.password}
                        helperText={errors.password ? errors.password?.message : ''}
                        fullWidth
                        margin="dense"
                    />
                )}
            />

            <Button variant="contained" sx={{ mt: 1 }} onClick={handleSubmit(onSubmit)} fullWidth>login</Button>
        </form>
    )
}

export default LoginCardPage
