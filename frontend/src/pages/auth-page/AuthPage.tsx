import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import { Box, Card, CardContent, Tab } from '@mui/material';
import React from 'react';
import LoginCardPage from './LoginCardPage';
import SignupCardPage from './RegisterCardPage';


const AuthPage: React.FC = () => {
    const [value, setValue] = React.useState('1');

    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue);
    };

    const login = <LoginCardPage />
    const register = <SignupCardPage />

    return (
        <Box sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>

            <Card sx={{ maxWidth: 360, alignSelf: 'center' }}>
                <CardContent>
                    <TabContext value={value}>
                        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                            <TabList onChange={handleChange} aria-label="lab API tabs example" variant="fullWidth">
                                <Tab label="LOG IN" value="1" />
                                <Tab label="SIGN UP" value="2" />
                            </TabList>
                        </Box>
                        <TabPanel value="1">{login}</TabPanel>
                        <TabPanel value="2">{register}</TabPanel>
                    </TabContext>
                </CardContent>
            </Card>
        </Box>
    );
}

export default AuthPage

