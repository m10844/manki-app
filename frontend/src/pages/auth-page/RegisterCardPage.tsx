
import { Button, TextField, Typography } from '@mui/material';
import { useForm, SubmitHandler, Controller } from 'react-hook-form';
import React, { useState } from 'react';
import * as conf from '../../config';
import { useNavigate } from 'react-router-dom';
import { register } from '../../api/APIUtils';
import { saveToken } from '../../utils/creditinalsUtils';
import paths from '../../routing/routes';


interface RegisterData {
    name: string,
    password: string,
    repeatPassword: string
}

const RegisterCardPage: React.FC = () => {
    const navigate = useNavigate();
    const [errorMessage, seterrorMessage] = useState<string>("")

    const {
        clearErrors,
        setError,
        control,
        handleSubmit,
        watch,
        formState: { errors },
    } = useForm<RegisterData>({
        defaultValues: {
            name: "",
            password: "",
            repeatPassword: ""
        }
    });

    const onSubmit: SubmitHandler<RegisterData> = (data) => {
        register({ username: data.name, password: data.password }).then(data => {
            if (data.token) {
                saveToken(data.token)
                navigate(paths.HOME)
                window.location.reload()
            } else {
                seterrorMessage(data.message)
            }
        })

    }

    return (
        <form>
            <Typography variant={'caption'} textAlign={'center'} color={'red'}>{errorMessage}</Typography>
            <Controller
                name="name"
                control={control}
                rules={{
                    required: {
                        value: true,
                        message: "Required field"
                    },
                    minLength: {
                        value: conf.NAME_MIN_LENGTH,
                        message: `Username must be at least ${conf.NAME_MIN_LENGTH}`
                    },
                    maxLength: {
                        value: conf.NAME_MAX_LENGTH,
                        message: "Too long username"
                    }
                }}
                render={({ field }) => (
                    <TextField
                        {...field}
                        label="Name"
                        variant="outlined"
                        error={!!errors.name}
                        helperText={errors.name ? errors.name?.message : ''}
                        fullWidth
                        margin="dense"
                    />
                )}
            />
            <Controller
                name="password"
                control={control}
                rules={{
                    required: {
                        value: true,
                        message: "Required field"
                    },
                    maxLength: {
                        value: conf.PASSWORD_MAX_LENGTH,
                        message: "Too long password"
                    },
                    minLength: {
                        value: conf.PASSWORD_MIN_LENGTH,
                        message: `Password must be at least ${conf.PASSWORD_MIN_LENGTH} symbols`
                    }
                }}
                render={({ field }) => (
                    <TextField
                        {...field}
                        type="password"
                        label="Password"
                        variant="outlined"
                        error={!!errors.password}
                        helperText={errors.password ? errors.password?.message : ''}
                        fullWidth
                        margin="dense"
                    />
                )}
            />
            <Controller
                name="repeatPassword"
                control={control}
                rules={{
                    required: {
                        value: true,
                        message: "Required field"
                    },
                    validate: (val: string) => {
                        if (watch('password') != val) {
                            return "Passwords do no match";
                        }
                    },
                }}
                render={({ field }) => (
                    <TextField
                        {...field}
                        type="password"
                        label="Repeat password"
                        variant="outlined"
                        error={!!errors.repeatPassword}
                        helperText={errors.repeatPassword ? errors.repeatPassword?.message : ''}
                        fullWidth
                        margin="dense"
                    />
                )}
            />
            <Button variant="contained" sx={{ mt: 1 }} onClick={handleSubmit(onSubmit)} fullWidth>submit</Button>
        </form>
    )
}

export default RegisterCardPage