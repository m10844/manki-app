import React from 'react';
import { Grid, LinearProgress, Typography } from '@mui/material';
import Box from '@mui/material/Box';
import { getDecks } from '../../api/APIUtils';
import { DeckListComponent, DeckListProps as DeckListSimplifiedResponse } from '../../components/Cards/DeckListComponent';
import { defaultLoadingWrapper, LoadingWrapper } from '../../types/LoadingWrapper';
import { LoadingState } from '../../types/LoadingState';
import { LoadingFailedComponent } from '../../components/LoadingFailedComponent';

export interface DeckInfo {
    id: number,
    deck_title: string,
    deck_description: string
    is_public: boolean
}

export const DeckListPage: React.FC = () => {
    const [deckWrapper, setData] = React.useState<LoadingWrapper<DeckListSimplifiedResponse>>({ state: LoadingState.LOADING, result: null, reason: null });
    
    React.useEffect(() => {
        getDecks().then(response => {
            if (response.status == 200) {
                setData({ state: LoadingState.LOADED, result: response.data, reason: null });
            }
        }).catch(
            error => {
                if (error.response.status == 403) {
                    setData({ state: LoadingState.FAILED, result: null, reason: "Access denied" });
                } else {
                    setData({ state: LoadingState.FAILED, result: null, reason: null });
                }

            }
        );
    }, []);


    switch (deckWrapper.state) {
        case LoadingState.LOADING: {
            return (<LinearProgress sx={{ m: 2 }} />);
        }
        case LoadingState.LOADED: {
            return (
                <Box sx={{ p: 3 }}>
                    <DeckListComponent deckInfo={deckWrapper.result.deckInfo} />
                </Box>
            )
        }
        case LoadingState.FAILED: {
            return (<LoadingFailedComponent reason={deckWrapper.reason} />);
        }
    }
}

const stub: DeckListSimplifiedResponse = {
    deckInfo: [
        {
            id: 1,
            deck_title: "deck 1",
            deck_description: "sample description",
            is_public: true
        },
        {
            id: 2,
            deck_title: "deck 2",
            deck_description: "sample description",
            is_public: true
        },
        {
            id: 3,
            deck_title: "deck 3",
            deck_description: "sample description",
            is_public: false
        },
        {
            id: 4,
            deck_title: "deck 4",
            deck_description: "sample description",
            is_public: false
        },
        {
            id: 5,
            deck_title: "deck 5",
            deck_description: "sample description",
            is_public: false

        },
        {
            id: 6,
            deck_title: "deck 6",
            deck_description: "sample description",
            is_public: false
        },
    ]
}

