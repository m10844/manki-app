import { ACCESS_TOKEN } from "../config"


export const saveToken = (token: string) => {
    localStorage.setItem(ACCESS_TOKEN, token)
}

export const removeToken = () => {
    localStorage.removeItem(ACCESS_TOKEN)
}

export const getToken = () => {
    localStorage.getItem(ACCESS_TOKEN)
}

export const isLoggedIn = () => {
    return localStorage.getItem(ACCESS_TOKEN) ? true : false
}