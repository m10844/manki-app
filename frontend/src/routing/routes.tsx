const paths = {
    HOME: '/',
    AUTH: 'auth',
    DECK_LIST: 'deck-list',
    DECK: 'deck/:deckId',
    SHARED_DECK: 'shared_deck/:deckToken',
    TRAIN: 'train',
    EDIT: 'edit',
    CARD_LIST: 'card-list',
    ADD_DECK: 'add_deck',
    CHANGE_PASSWORD: 'change_password',
    ANY: '*',
    INPUT_TRAIN: 'input_train',
    TIMED_TRAIN: 'timed_train',
    STAT_TRAIN: 'stat_train',
    CHOOSE_TRAIN_MODE: 'choose_mode',
    ABOUT: 'about'
};

export default paths;
