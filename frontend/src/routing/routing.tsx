import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import paths from './routes';
import HomePage from '../pages/home-page/HomePage';
import AuthPage from '../pages/auth-page/AuthPage';

import TrainPage from '../pages/train-page/TrainPage';
import RootWrapper from '../components/RootWrapper';
import { DeckLoaderComponent } from '../components/DeckLoaderComponent';
import { DeckListPage } from '../pages/deck-list-page/DeckListPage';
import { EditDeckPage } from '../pages/edit-deck-page/EditDeckPage';
import { DetailedDeckPage } from '../pages/detailed-deck-page/DetailedDeckPage';
import { NotFoundPage } from '../pages/not-found-page/NotHoundPage';
import { AddDeckPage } from '../pages/add-new-deck-page/AddDeckPage';
import ChangePasswordPage from '../pages/change-password/ChangePasswordPage';

import ChooseTrainingPage from '../pages/choose-training-page/ChooseTrainingPage';
import InputTrainPage from '../pages/input-train-page/InputTrainPage';
import StatisticsTrainPage from '../pages/statistics-train-page/StatisticsTrainPage';
import TimedTrainPage from '../pages/timed-train-page/TimedTrainPage';
import AboutPage from '../pages/about_page/AboutPage'

const getRoutes = () => {
    return (
        <Routes >
            <Route path="/" element={<RootWrapper />}>
                <Route index element={<HomePage />} />
                <Route path={paths.AUTH} element={<AuthPage />} />
                <Route path={paths.DECK_LIST} element={<DeckListPage />} />
                <Route path={paths.ADD_DECK} element={<AddDeckPage />} />
                <Route path={paths.ABOUT} element={<AboutPage />} />
                <Route path={'reload'} element={<Navigate to={`/${paths.DECK_LIST}`} replace />} />
                <Route path={paths.DECK} element={<DeckLoaderComponent />}>
                    <Route index element={<Navigate to={paths.CARD_LIST} replace />} />
                    <Route path={paths.CHOOSE_TRAIN_MODE} element={<ChooseTrainingPage />} />
                    <Route path={paths.TRAIN} element={<TrainPage />} />
                    <Route path={paths.INPUT_TRAIN} element={<InputTrainPage />} />
                    <Route path={paths.TIMED_TRAIN} element={<TimedTrainPage />} />
                    <Route path={paths.STAT_TRAIN} element={<StatisticsTrainPage/>} />
                    <Route path={paths.EDIT} element={<EditDeckPage />} />
                    <Route path={paths.CARD_LIST} element={<DetailedDeckPage />} />
                </Route>
                <Route path={paths.CHANGE_PASSWORD} element={<ChangePasswordPage />} />
                <Route path={paths.SHARED_DECK} element={<DeckLoaderComponent />}>
                    <Route index element={<Navigate to={paths.CARD_LIST} replace />} />
                    <Route path={paths.TRAIN} element={<TrainPage />} />
                    <Route path={paths.EDIT} element={<EditDeckPage />} />
                    <Route path={paths.CARD_LIST} element={<DetailedDeckPage />} />
                </Route>
                <Route path={paths.ANY} element={<NotFoundPage />} />
            </Route>
        </Routes >
    );
}

export default getRoutes
