
export interface MankiCard {
    id: number,
    question: string,
    answer: string,
    knowledge_points: number
}

export interface MankiDeck {
    deck: {
        id: number,
        cards: MankiCard[],
        deck_title: string,
        deck_description: string,
        is_public: boolean
    }
}
