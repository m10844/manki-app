export enum LoadingState {
    LOADING,
    LOADED,
    FAILED
}