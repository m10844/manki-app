import { LoadingState } from "./LoadingState";

export interface LoadingWrapper<ResultType> {
    result: ResultType | null
    state: LoadingState
    reason: string | null
}

export const defaultLoadingWrapper = <ResultType>(): LoadingWrapper<ResultType> => {
    return {
        result: null,
        state: LoadingState.LOADING,
        reason: null
    }
}