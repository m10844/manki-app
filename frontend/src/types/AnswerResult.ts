export enum AnswerResult {
    FAIL,
    HARD,
    GOOD,
    EASY
}
