import * as React from 'react';
import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import paths from '../routing/routes';
import { isLoggedIn } from '../utils/creditinalsUtils';
import { useLocation, useNavigate } from 'react-router-dom';
import { Container } from '@mui/material';

interface LinkTabProps {
    label?: string;
    href?: string;
}

function LinkTab(props: LinkTabProps) {
    return (
        <Tab
            component="a"
            {...props}
        />
    );
}

export default function NavTabs() {
    const [value, setValue] = React.useState(paths.HOME);
    const location = useLocation();
    const navigate = useNavigate();

    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue);
        navigate(newValue);
    };

    if (isLoggedIn()) {
        return (
            <Container sx={{ p: 1 }} >
                <Tabs value={location.pathname.substring(1)}
                    onChange={handleChange}
                    textColor="secondary"
                    indicatorColor="secondary">
                        
                    <Tab value={paths.HOME} label="Home" />
                    <Tab value={paths.DECK_LIST} label={"My Decks"} />
                    <Tab value={paths.ADD_DECK} label={"Add New Deck"} />
                    <Tab value={paths.ABOUT} label={"About"} />
                </Tabs>
            </Container>
        );
    } else {
        return (<div></div>);
    }
}
