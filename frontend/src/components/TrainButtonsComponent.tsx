import React from "react";
import { Button, Grid } from "@mui/material";
import Box from "@mui/material/Box";
import { AnswerResult } from "../types/AnswerResult";


interface TrainButtonsProps {
  onAnswer: (answer: AnswerResult) => void,
  enableFail: boolean
}

const TrainButtonsComponent: React.FC<TrainButtonsProps> = ({ onAnswer, enableFail }) => {
  const getFail = (flag: boolean) => {
    if (flag) {
      return (
        <Grid item xs={2} sm={4} md={3}>
          <Button fullWidth variant="contained" color="error" onClick={() => { onAnswer(AnswerResult.FAIL) }}>
            Fail
          </Button>
        </Grid>
      )
    } else {
      return undefined
    }
  }

  return (
    <Box
      sx={{ p: 2 }}>
      <Grid container spacing={{ xs: 2, md: 3 }}>
        {getFail(enableFail)}

        <Grid item xs={2} sm={4} md={3}>
          <Button fullWidth variant="contained" color="warning" onClick={() => { onAnswer(AnswerResult.HARD) }}>
            Hard
          </Button>
        </Grid>

        <Grid item xs={2} sm={4} md={3}>
          <Button fullWidth variant="contained" color="info" onClick={() => { onAnswer(AnswerResult.GOOD) }}>
            Good
          </Button>
        </Grid>

        <Grid item xs={2} sm={4} md={3}>
          <Button fullWidth variant="contained" color="success" onClick={() => { onAnswer(AnswerResult.EASY) }}>
            Easy
          </Button>
        </Grid>
      </Grid>
    </Box>
  );
};

export default TrainButtonsComponent
