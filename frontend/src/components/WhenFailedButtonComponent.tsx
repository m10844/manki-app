import React from "react";
import { Button, Grid } from "@mui/material";
import Box from "@mui/material/Box";
import { AnswerResult } from "../types/AnswerResult";


interface TrainButtonsProps {
  onAnswer: (answer: AnswerResult) => void
}

const WhenFailedButtonComponent: React.FC<TrainButtonsProps> = ({ onAnswer }) => {

  return (
    <Box sx={{ p: 2 }}>
        <Button fullWidth variant="contained" color="error" onClick={() => { onAnswer(AnswerResult.FAIL) }}>
        Next
        </Button>
    </Box>
  );
};

export default WhenFailedButtonComponent
