import { Avatar, Button, List, ListItem, ListItemAvatar, ListItemText, Typography } from '@mui/material';
import Box from '@mui/system/Box/Box';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { getProfile, getSampleText, http, myRequest } from '../api/APIUtils';
import "../styles/SampleStyle.css";
import AssignmentTurnedInIcon from '@mui/icons-material/AssignmentTurnedIn';
import CollectionsBookmarkIcon from '@mui/icons-material/CollectionsBookmark';

const UserInfoComponent: React.FC = () => {

    const [userInfo, setUserInfo] = React.useState<any>({});

    React.useEffect(() => {
        getProfile().then(data => setUserInfo(data));
    }, []);
    console.log(userInfo)
    

    return (
        <Box sx={{ p: 3 }}>
            {(userInfo)
                ?
                <Box>
                    <Typography variant="h5" sx={{ textTransform: 'capitalize', fontWeight: 'bold' }}>
                        Hi {userInfo.name}
                    </Typography>
                    <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
                        <ListItem>
                            <ListItemAvatar>
                                <Avatar>
                                    <AssignmentTurnedInIcon />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary="Average Knowlege points:" secondary={userInfo.averageKnowledgePoints} />
                        </ListItem>
                        <ListItem>
                            <ListItemAvatar>
                                <Avatar>
                                    <CollectionsBookmarkIcon />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary="Your total decks:" secondary={userInfo.totalDecks} />
                        </ListItem>
                    </List>
                </Box>
                :
                <span />
            }
        </Box>
    );
}

export default UserInfoComponent