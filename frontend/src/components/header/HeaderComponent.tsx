import { Logout, PersonAdd, Settings } from '@mui/icons-material';
import { AppBar, Avatar, Button, Divider, IconButton, ListItemIcon, Menu, MenuItem, Toolbar, Tooltip, Typography } from '@mui/material';
import { deepOrange } from '@mui/material/colors';
import { Box } from '@mui/system';
import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { getProfile } from '../../api/APIUtils';
import paths from '../../routing/routes';
import { isLoggedIn, removeToken } from '../../utils/creditinalsUtils';
import NavTabs from '../NavigationTabsComponent';

const HeaderComponent: React.FC = () => {
    const loggedIn = isLoggedIn()
    const navigate = useNavigate();

    const loginListener = () => {
        navigate(paths.AUTH);
    }

    const logoutListener = () => {
        removeToken()
        navigate(paths.HOME)
        window.location.reload()
    }

    const [firstLetter, setFirstLetter] = React.useState("");

    React.useEffect(() => {
        getProfile().then(data => {
            setFirstLetter(data.name[0].toUpperCase());
        });
    }, []);

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    let rightSide = <Button color={'inherit'} onClick={loginListener}>LOG IN / SIGN UP</Button>;
    if (loggedIn) {
        rightSide = (
            <>
                <Tooltip title="Account settings">
                    <IconButton
                        onClick={handleClick}
                        size="small"
                        sx={{ ml: 2 }}
                        aria-controls={open ? 'account-menu' : undefined}
                        aria-haspopup="true"
                        aria-expanded={open ? 'true' : undefined}
                    >
                        <Avatar sx={{ width: 32, height: 32, bgcolor: deepOrange[400] }}>{firstLetter}</Avatar>
                    </IconButton>
                </Tooltip>
                <Menu
                    anchorEl={anchorEl}
                    id="account-menu"
                    open={open}
                    onClose={handleClose}
                    onClick={handleClose}
                    PaperProps={{
                        elevation: 0,
                        sx: {
                            overflow: 'visible',
                            filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                            mt: 1.5,
                            '& .MuiAvatar-root': {
                                width: 32,
                                height: 32,
                                ml: -0.5,
                                mr: 1,
                            },
                            '&:before': {
                                content: '""',
                                display: 'block',
                                position: 'absolute',
                                top: 0,
                                right: 14,
                                width: 10,
                                height: 10,
                                bgcolor: 'background.paper',
                                transform: 'translateY(-50%) rotate(45deg)',
                                zIndex: 0,
                            },
                        },
                    }}
                    transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                    anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
                >
                    <Link to={paths.CHANGE_PASSWORD} style={{ textDecoration: 'none', color: 'inherit' }}>
                        <MenuItem>
                            <ListItemIcon>
                                <Settings fontSize="small" />
                            </ListItemIcon>
                            Change password
                        </MenuItem>
                    </Link>
                    <MenuItem onClick={logoutListener}>
                        <ListItemIcon>
                            <Logout fontSize="small" />
                        </ListItemIcon>
                        Logout
                    </MenuItem>
                </Menu>
            </>
        );
    }

    return (
        <div>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h4" component="div" sx={{ p: 2, flexGrow: 1, fontWeight: 'bold' }}>
                        <Link to={paths.HOME} style={{ textDecoration: 'none', color: 'inherit' }}>Manki App</Link>
                    </Typography>
                    {rightSide}
                </Toolbar>
            </AppBar >
            <NavTabs />
        </div>
    )
}

export default HeaderComponent

