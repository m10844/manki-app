import { Box, Container, Divider, Grid, Typography } from '@mui/material';
import React from 'react';

const FooterComponent: React.FC = () => {
    return (
        <div>
            <Divider />
            <Box sx={{ display: `flex`, justifyContent: `center` }}>
                <Typography variant={"caption"} align={'center'} sx={{ mt: "24px", mb: "32px", color: 'gray', maxWidth: `360px` }}>
                    Powered by peachblack, romchirik, dariakhaetskaya, Dmitrii Makogon, Andrei Isachenko
                </Typography>
            </Box>
        </div>
    )
}

export default FooterComponent  