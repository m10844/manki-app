import { Container, createTheme, ThemeProvider } from '@mui/material';
import React from 'react';
import { Outlet } from 'react-router-dom';
import FooterComponent from './footer/FooterComponent';
import HeaderComponent from './header/HeaderComponent';

const theme = createTheme({
    palette: {
        primary: {
            light: '#00897b',
            main: '#005f56',
            contrastText: '#FFFFFF'
        },
        secondary: {
            light: '#a3b3a3',
            main: '#8ca18c',
            dark: '#627062',
            contrastText: '#FFFFFF'
        },
        error: {
            main: '#a73a38',
            contrastText: '#FFFFFF'
        },
        info: {
            main: '#ffb300',
            contrastText: '#FFFFFF'
        },
        warning: {
            main: '#ff7043',
            contrastText: '#FFFFFF'
        },
        success: {
            main: '#689f38',
            contrastText: '#FFFFFF'
        }
    }
});

const RootWrapper: React.FC = () => {
    return (<div>
        <ThemeProvider theme={theme}>
            <HeaderComponent />
            <Container>
                <Outlet />
            </Container>
            <FooterComponent />
        </ThemeProvider>
    </div>);
}

export default RootWrapper