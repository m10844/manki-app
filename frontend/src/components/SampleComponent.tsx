import { Button } from '@mui/material';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { getSampleText, http, myRequest } from '../api/APIUtils';
import "../styles/SampleStyle.css";

type CompProps = { num: number };

export default function SampleComponent({ num }: CompProps) {

  const [counter, setCounter] = React.useState(0);
  const [sampleText, setSampleText] = React.useState("");

  React.useEffect(() => {
    getSampleText().then(data => setSampleText(data));
  }, []);

  return (
    <>
      <h1 data-testid="counter" className='sample-header'>Hello world React! Num: {num}</h1>
      <Button onClick={() => setCounter(counter + 1)} variant="contained">{counter}</Button>
      <p data-testid="sample-text">{sampleText}</p>
    </>
  );
}