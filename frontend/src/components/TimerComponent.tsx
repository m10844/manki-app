import { Typography } from "@mui/material";
import * as React from "react";
import { useState } from "react";

export const TimerComponent: React.FC = () => {
    const [seconds, setSec] = useState<number>(0)
    const [minutes, setMin] = useState<number>(0)

    const withLeadZeros = (n: number) => String(n).padStart(2, '0');

    React.useEffect(() => {
        setTimeout(() => {
            let tmpSec = seconds + 1;
            let tmpMin = minutes;

            if (tmpSec == 60) {
                tmpSec = 0;
                tmpMin = minutes + 1;
            }
            if (tmpMin == 60) {
                tmpMin = 0;
            }
            setSec(tmpSec);
            setMin(tmpMin)
        }, 1000)
    }, [minutes, seconds])

    return (
        <Typography sx={{ textAlign: `end` }} variant="h5" gutterBottom>{`Time: ${withLeadZeros(minutes)}:${withLeadZeros(seconds)}`}</Typography>
    );
}