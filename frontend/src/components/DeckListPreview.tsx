import React from 'react';
import { Grid, LinearProgress, Typography } from '@mui/material';
import Box from '@mui/material/Box';
import { LoadingWrapper } from '../types/LoadingWrapper';
import { LoadingState } from '../types/LoadingState';
import { getDecks } from '../api/APIUtils';
import { DeckListComponent, DeckListProps as DeckListSimplifiedResponse } from './Cards/DeckListComponent';
import { LoadingFailedComponent } from './LoadingFailedComponent';


export interface DeckInfo {
    id: number,
    deck_title: string,
    deck_description: string
}

export const DeckListPreview: React.FC = () => {
    const [deckWrapper, setData] = React.useState<LoadingWrapper<DeckListSimplifiedResponse>>({ state: LoadingState.LOADING, result: null, reason: null });

    React.useEffect(() => {
        getDecks().then(response => {
            if (response.status == 200) {
                setData({ state: LoadingState.LOADED, result: response.data, reason: null });
            }
        }).catch(
            error => {
                if (error.response.status == 403) {
                    setData({ state: LoadingState.FAILED, result: null, reason: "Access denied" });
                } else {
                    setData({ state: LoadingState.FAILED, result: null, reason: null });
                }

            }
        );
    }, []);


    switch (deckWrapper.state) {
        case LoadingState.LOADING: {
            return (<LinearProgress sx={{ m: 2 }} />);
        }
        case LoadingState.LOADED: {
            if (deckWrapper.result.deckInfo.length > 0){
                return (
                                <Box sx={{ bgcolor: 'secondary.light', p : 1}}>
                                    <Typography variant="h6" sx={{ textTransform: 'capitalize', fontWeight: 'bold', color: 'white' }}>
                                        These are your recent decks:
                                    </Typography>
                                    <DeckListComponent deckInfo={deckWrapper.result.deckInfo.slice(0, 3)} />
                                </Box>
                            )
            } else {
                return <span/>
            }
            
        }
        case LoadingState.FAILED: {
            return (<LoadingFailedComponent reason={deckWrapper.reason} />);
        }
    }
}
