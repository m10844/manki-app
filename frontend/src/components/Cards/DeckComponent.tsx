import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import {CardActionArea} from '@mui/material';
import Fab from '@mui/material/Fab';
import { SxProps } from '@mui/system';


export interface DeckParams {
    title: string,
    description: string
    size: number
}

const fabStyle = {
  position: 'absolute',
  bottom: 16,
  right: 16,
  boxShadow: 0
};

const fab = {
  sx: fabStyle as SxProps,
  color: 'secondary.light' as 'secondary'
};

const CardComponent: React.FC<DeckParams> = ({ title, description, size}) => {
  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
          {title}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {description}
          </Typography>
          <Fab variant="extended" size="medium" color={fab.color} aria-label="add" sx={fab.sx}>
            {size}
          </Fab>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

export default CardComponent
