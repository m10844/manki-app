import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { Button, CardActionArea, Dialog, Divider, DialogActions, DialogContent, DialogContentText, DialogTitle, Grid, IconButton, Badge } from '@mui/material';
import Fab from '@mui/material/Fab';
import { Box, SxProps } from '@mui/system';
import DeleteIcon from '@mui/icons-material/Delete';
import { useNavigate } from 'react-router-dom';
import paths from '../../routing/routes';
import { isConstructorDeclaration } from 'typescript';
import { deleteDeck, shareDeck } from '../../api/APIUtils';
import { PUBLIC_URL } from '../../config';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';

export interface DeckParams {
    id: number,
    title: string,
    description: string
    size: number
    is_public: boolean
}

const fabStyle = {
    position: 'absolute',
    bottom: 16,
    right: 16,
    boxShadow: 0
};

const fab = {
    sx: fabStyle as SxProps,
    color: 'secondary.light' as 'secondary'
};

const DeckDetailsCard: React.FC<DeckParams> = ({ id, title, description, size, is_public }) => {
    const navigate = useNavigate();
    const [a, forceUpdate] = React.useState<number>(1);
    const [open, setOpen] = React.useState(false);
    const [shareLink, setShareLink] = React.useState("");

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const onDeleteDeck = (id: number) => {
        deleteDeck(id).then(response => {
            if (response.status == 200) {
                navigate(`/reload/`);
            }
        }
        ).catch(
            error => { console.log(`failed to delete deck`) }
        )
    }

    const onDeckShare = () => {
        shareDeck(id).then(r => {
            setOpen(true);
            setShareLink(`${PUBLIC_URL}/shared_deck/${r.data.share_token}`);
        })
    }
    
    const is_public_string = (publ: boolean) => {
        if (publ) {
            return "public"
        }
        return ""
    } 

    return (
        <>  
            <Card sx={{ maxWidth: 360 }} >
                <CardActionArea disableRipple>
                    <CardContent>
                        <Grid container spacing={2}>
                            <Grid item xs={10}>
                                <Typography gutterBottom variant="h5" component="div">
                                    {title}
                                </Typography>
                            </Grid>

                            <Grid item xs={2}>
                                <Typography sx={{ fontSize: 12, textAlign: "right" }} color="text.secondary" gutterBottom>
                                    {is_public_string(is_public)}
                                </Typography>
                            </Grid>
                        </Grid>
                

                        <Typography gutterBottom variant="body2" color="text.secondary">
                            {description}
                        </Typography>

                        <Grid
                            container
                            wrap={`nowrap`}
                            direction="row"
                            justifyContent="space-between"
                            alignItems="center"
                            sx={{ mt: 1 }}>
                            <Button variant={`outlined`} sx={{ mr: 1 }} onClick={() => { navigate(`/${'deck'}/${id}/${paths.EDIT}`) }}>EDIT</Button>
                            <Button variant={`outlined`} sx={{ mr: 1 }} onClick={() => { navigate(`/${'deck'}/${id}/${paths.CARD_LIST}`) }}>DETAILS</Button>
                            <Button variant={`outlined`} sx={{ mr: 1 }} onClick={() => {
                                if (is_public){
                                    navigate(`/${'deck'}/${id}/${paths.STAT_TRAIN}`)
                                } else {
                                    navigate(`/deck/${id}/${paths.CHOOSE_TRAIN_MODE}`) 
                                } 
                                 }}> TRAIN</Button>
                            <Button variant={`outlined`} sx={{ mr: 1 }} onClick={onDeckShare}>SHARE</Button>
                            <IconButton aria-label="delete" sx={{ mr: 1 }} onClick={() => { onDeleteDeck(id) }}> <DeleteIcon /></IconButton>
                        </Grid>
                    </CardContent>
                </CardActionArea >
            </Card >
            <Box sx={{ width: 700 }}>
                <Dialog
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">
                        {"Deck share"}
                    </DialogTitle>
                    <DialogContent>
                        <Stack spacing={2}>
                            <Divider />
                            <TextField
                                id="outlined-read-only-input"
                                label="Share link"
                                defaultValue={shareLink}
                                InputProps={{
                                    readOnly: true,
                                }}
                            />
                            <Button variant={`outlined`} onClick={() => { navigator.clipboard.writeText(shareLink) }}>Copy</Button>
                        </Stack>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>Close</Button>
                    </DialogActions>
                </Dialog>
            </Box>
        </>
    );
}

export default DeckDetailsCard
{/* <Fab variant="extended" size="medium" color={fab.color} aria-label="add" sx={fab.sx}>
                        {size}
                    </Fab> */}
