import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import React from "react";
import { useNavigate } from "react-router-dom";
import { DeckInfo } from "../../pages/deck-list-page/DeckListPage";
import DeckDetailsCard from "./DeckDetailsCard";



export interface DeckListProps {
    deckInfo: DeckInfo[]
}


export const DeckListComponent: React.FC<DeckListProps> = ({ deckInfo }) => {
    return (
        <Box sx={{ p: 1 }}>
            <Grid
                container
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 2, sm: 8, md: 12 }}>
                {[deckInfo.map(
                    (deck) =>
                        <Grid item xs={2} sm={4} md={4}
                            key={deck.id.toString()}>
                            <DeckDetailsCard
                                id={deck.id}
                                title={deck.deck_title}
                                description={deck.deck_description}
                                size={10}
                                is_public={deck.is_public} />
                        </Grid>
                )]}
            </Grid>
        </Box>
    );
}
