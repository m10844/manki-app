import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";

export interface CardInfo {
    bodyText: string,
    deckName: string,
    knowledgePoints: number
}

const MankiCard: React.FC<CardInfo> = ({bodyText, deckName, knowledgePoints}) => {

  const getColor = (points: number) => {
      if (points < 4){
          return '#a73a38'
      }else if (points < 8){
          return '#ff7043'
      } else if (points < 12) {
          return '#ffb300'
      } else if (points >= 12){
          return '#689f38'
      }
  }

  return (
      <Card sx={{ minWidth: '100%' }}>
        <CardContent>
        <Grid container spacing={2}>

          <Grid item xs={8}>
            <Typography sx={{ fontSize: 12 }} color="text.secondary" gutterBottom>
              {deckName}
            </Typography>
          </Grid>

          <Grid item xs={4}>
            <Typography sx={{ fontSize: 12, textAlign: "right" }} color={getColor(knowledgePoints)} gutterBottom>
              Knowlege points: {knowledgePoints}
            </Typography>
          </Grid>

        </Grid>

          <Typography sx={{ textAlign: "center" }} variant="h5" component="div">
            {bodyText}
          </Typography>
        </CardContent>
      </Card>
  );
}

export default MankiCard
