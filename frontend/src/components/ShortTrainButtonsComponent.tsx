import React from "react";
import { Button, Grid } from "@mui/material";
import Box from "@mui/material/Box";
import { AnswerResult } from "../types/AnswerResult";


interface TrainButtonsProps {
  onAnswer: (answer: AnswerResult) => void
}

const ShortTrainButtonsComponent: React.FC<TrainButtonsProps> = ({ onAnswer }) => {

  return (
    <Box sx={{ p: 2 }}>
      <Grid container spacing={{ xs: 2, md: 2 }}>
        <Grid item xs={2} sm={4} md={4}>
          <Button fullWidth variant="contained" color="warning" onClick={() => { onAnswer(AnswerResult.HARD) }}>
            Hard
          </Button>
        </Grid>

        <Grid item xs={2} sm={4} md={4}>
          <Button fullWidth variant="contained" color="info" onClick={() => { onAnswer(AnswerResult.GOOD) }}>
            Good
          </Button>
        </Grid>

        <Grid item xs={2} sm={4} md={4}>
          <Button fullWidth variant="contained" color="success" onClick={() => { onAnswer(AnswerResult.EASY) }}>
            Easy
          </Button>
        </Grid>
      </Grid>
    </Box>
  );
};

export default ShortTrainButtonsComponent
