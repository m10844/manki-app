import React, { useEffect, useState } from 'react';
import { Outlet, useOutletContext, useParams } from 'react-router-dom';
import { LoadingState } from '../types/LoadingState';
import { MankiDeck } from '../types/MankiDeck';
import { LoadingWrapper } from '../types/LoadingWrapper';
import { getDeckById, getSharedDeck } from '../api/APIUtils';



export const DeckLoaderComponent: React.FC = () => {

    const [deck, setDeck] = React.useState<LoadingWrapper<MankiDeck>>({ state: LoadingState.LOADING, result: null, reason: null });
    const { deckId, deckToken } = useParams();
    const [id] = useState<number>(Number(deckId));
    const [token] = useState<string>(deckToken);

    useEffect(() => {
        if (isNaN(id))
            return;
        getDeckById(id).then(response => {
            if (response.status == 200) {
                setDeck({ state: LoadingState.LOADED, result: response.data, reason: null });
            }
        }).catch(
            error => {
                if (error.response.status == 403) {
                    setDeck({ state: LoadingState.FAILED, result: null, reason: "Access denied" });
                } else {
                    setDeck({ state: LoadingState.FAILED, result: null, reason: null });
                }

            }
        );
    }, [id]);

    useEffect(() => {
        if (token === undefined)
            return;

        getSharedDeck(token).then(r => {
            if (r.status == 200) {
                setDeck({ state: LoadingState.LOADED, result: r.data, reason: null });
            }
        }).catch(
            error => {
                if (error.response.status == 403) {
                    setDeck({ state: LoadingState.FAILED, result: null, reason: "Access denied" });
                } else {
                    setDeck({ state: LoadingState.FAILED, result: null, reason: null });
                }

            }
        );
    }, [token]);

    return (
        <Outlet context={deck} />
    )
}

export function useDeck(): LoadingWrapper<MankiDeck> {
    return useOutletContext<LoadingWrapper<MankiDeck>>();
}

const stub: MankiDeck = {
    deck: {
        id: 30,
        cards: [
            {
                id: 0,
                question: "Where fazan hides?",
                answer: "red",
                knowledge_points: 0
            },
            {
                id: 1,
                question: "Where fazan hides?",
                answer: "orange",
                knowledge_points: 0
            },
            {
                id: 2,
                question: "Where fazan hides?",
                answer: "yellow",
                knowledge_points: 0
            },
            {
                id: 3,
                question: "Where fazan hides?",
                answer: "green",
                knowledge_points: 0
            }, {
                id: 4,
                question: "Where fazan hides?",
                answer: "blue",
                knowledge_points: 0
            }, {
                id: 5,
                question: "Where fazan hides?",
                answer: "aboba",
                knowledge_points: 0
            }
        ],
        deck_title: "Where fazan hides?",
        deck_description: "aboba18_descr",
        is_public: true
    }
}