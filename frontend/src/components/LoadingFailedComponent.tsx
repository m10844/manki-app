import { Box, Typography } from '@mui/material';
import React from 'react';

interface ComponentProps {
    reason: string | null
}

export const LoadingFailedComponent: React.FC<ComponentProps> = ({ reason }) => {
    const reasonFormatted = `Reason: ${(reason || "Unknown")}`;
    return (
        <Box sx={{ display: `flex`, flexDirection: `column`, justifyContent: `center`, height: `350px` }}>
            <Typography textAlign={`center`} variant="h3">Failed to load content</Typography>
            <Typography textAlign={`center`} variant="h5">{reasonFormatted}</Typography>
        </Box>
    )
}