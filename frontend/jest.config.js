/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jest-environment-jsdom',
  moduleNameMapper: {
    "\\.(css|less)$": "<rootDir>/static/css/__mocks__/styleMock.js"
  },
  setupFilesAfterEnv: ['./src/setupTests.tsx'],
  modulePathIgnorePatterns: ["<rootDir>/build/"]
};