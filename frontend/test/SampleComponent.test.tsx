import SampleComponent from "../src/components/SampleComponent";
import * as React from "react";
import { render, waitFor, screen, cleanup, findByTestId, waitForElementToBeRemoved, RenderResult } from '@testing-library/react';
import { http } from "../src/api/APIUtils";
import { act } from "react-dom/test-utils";

afterEach(() => {
  jest.clearAllMocks();
  cleanup();
});

// https://testing-library.com/docs/react-testing-library/example-intro
// https://www.freecodecamp.org/news/8-simple-steps-to-start-testing-react-apps-using-react-testing-library-and-jest/
it("renders with a given number and resolved request", async () => {
  var someText = 'Some text';
  const mockData = {
    'data': someText
  };
  let num = 42;
  let renderRes : RenderResult;
  await act(async () => {
    jest.spyOn(http, 'request').mockResolvedValue(mockData);
    renderRes = render(<SampleComponent num={num} />);
  });
  const { findByTestId } = renderRes;
  var counter = await findByTestId("counter");
  var sampleText = await findByTestId("sample-text");
  expect(counter.textContent).toBe("Hello world React! Num: " + num.toString());
  // expect(sampleText.textContent).toBe(someText);
});
